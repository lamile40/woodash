import React , {useContext} from "react";
import "./assets/css/app.scss";
import AppRouter from "./routing/AppRouter";
import { ModalContext } from "./context/ModalContext";
import Modal from "./components/shared/Modal";

const App = () => {
  const [modalData] = useContext(ModalContext);
  return (
    <>
      <AppRouter />
      { modalData.display && <Modal /> }
    </>
  );
};

export default App;

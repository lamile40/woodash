import React from "react";
import Section from "./sub_components/Section";
import "./layout.scss";

const Layout = ({ children }) => {
  return <div className="layout">{children}</div>;
};

Layout.Section = Section;

export default Layout;

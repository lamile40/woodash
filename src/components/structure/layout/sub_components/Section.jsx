import React from "react";
import cx from "classnames/bind";

const Section = ({ children, secondary, oneHalf, oneThird, sticky }) => {
  return (
    <div
      style={ sticky && {position: 'sticky', top: sticky}}
      className={cx({
        layout__section: true,
        "layout__section--secondary": secondary,
        "layout__section--oneHalf": oneHalf,
        "layout__section--oneThird": oneThird,
      })}
    >
      {children}
    </div>
  );
};

export default Section;

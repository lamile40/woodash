import React from 'react'
import './style.scss'
import cx from 'classnames/bind'
const DisplayText = ({ children, large, success, negative }) => {
    return (
        <div className={cx({
            "display-text": true,
            "display-text--large": large,
            "display-text--success": success,
            "display-text--negative": negative,
        })}>
            {children}
        </div>
    )
}

export default DisplayText

import React from "react";
import "./style.scss";

const InputElement = ({ label, id, onChange, value }) => {
  return (
    <div className="form-element form-element--input">
      <label htmlFor={id}>{label}</label>
      <input
        type="text"
        onChange={(e) => onChange(e.target.value)}
        value={value}
      />
    </div>
  );
};

export default InputElement;

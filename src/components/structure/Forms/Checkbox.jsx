import React from "react";
import cx from "classnames/bind";

const Checkbox = ({ id, label, checked, onChange }) => {
  return (
    <div className="form-element form-element--checkbox">
      <label
        className={cx({
          checkbox: true,
        })}
        htmlFor={id}
      >
        <span
          className={cx({
            checkbox__control: true,
            "checkbox__control--checked": checked,
          })}
        >
          <input
            type="checkbox"
            id={id}
            checked={checked}
            onChange={(e) => onChange(e.target.checked)}
          />
        </span>
        <span className="checkbox__label">{label}</span>
      </label>
    </div>
  );
};

export default Checkbox;

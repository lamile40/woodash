import React, { useMemo, useState } from "react";
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState, convertToRaw} from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import '../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const RichEditor = () => {

  const [editorState, seteditorState] = useState(EditorState.createEmpty())

  const onEditorStateChange = (EditorState) => {
      seteditorState(EditorState)
  };

  return (
    <div className="form-element">
      <label htmlFor="">Description</label>
      <div style={{padding: 8, border: '2px solid #eee', borderRadius: 4}}>
        <Editor
            editorState={editorState}
            toolbarClassName="rdw-storybook-toolbar"
            wrapperClassName="rdw-storybook-wrapper"
            editorClassName="rdw-storybook-editor"
            toolbar={{
                inline: { inDropdown: true },
                list: { inDropdown: true },
                textAlign: { inDropdown: true },
                link: { inDropdown: true },
                history: { inDropdown: true },
              }}
            onEditorStateChange={onEditorStateChange}
        />
      </div>
    </div>
  );
};

export default RichEditor;

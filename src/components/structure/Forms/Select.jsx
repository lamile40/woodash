import React, { useEffect, useState } from "react";

const Select = ({ label, options, onChange, value, id, name }) => {
  const [selectedValue, setSelectedValue] = useState();

  useEffect(() => {
    setSelectedValue(options.filter((opt) => opt.value === value).shift());
    return () => {};
  }, [value]);

  return (
    <div className="form-element form-element--select">
      <label htmlFor={id}>{label}</label>
      <select name={name} id={id} onChange={onChange}>
        {options.map((option, index) => (
          <option key={index} value={option.value}>{option.label}</option>
        ))}
      </select>
      <div className="select-content">
        <div className="select-content__content">
          <span>{selectedValue && selectedValue.label}</span>
          <i className="fas fa-arrows-alt-v" />
        </div>
        <div className="select-backdrop"></div>
      </div>
    </div>
  );
};

export default Select;

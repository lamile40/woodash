import React from "react";

const FormLayoutGroup = ({ children }) => {
  return <div className="form-layout__grouped">{children}</div>;
};

export default FormLayoutGroup;

import React from "react";
import FormLayoutGroup from "./FormLayoutGroup";

const FormLayout = ({ children }) => {
  return <div className="form-layout">{children}</div>;
};

FormLayout.Group = FormLayoutGroup;

export default FormLayout;

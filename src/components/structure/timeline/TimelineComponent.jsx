import React from 'react'
import SkeletonDisplayText from '../ActivityIndicator/SkeletonDisplayText'

export const LoadingComponent = () => {
    return (
        <>
            <li className="timeline__item">
                <SkeletonDisplayText size="mini" />
                <SkeletonDisplayText lines="2" fullW />
            </li>
            <li className="timeline__item">
                <SkeletonDisplayText size="mini" />
                <SkeletonDisplayText lines="2" fullW />
            </li>
            <li className="timeline__item">
                <SkeletonDisplayText size="mini" />
                <SkeletonDisplayText lines="2" fullW />
            </li>
            <li className="timeline__item">
                <SkeletonDisplayText size="mini" />
                <SkeletonDisplayText lines="2" fullW />
            </li>
        </>

    )
}

export const TimelineComponent = (props) => {
    return (
        <ul className="timeline">
            { props.items ?
                <>
                    {props.items.map((item ,index)=> (
                        <li key={`timeline-${index}`} className="timeline__item">
                            <p className="timeline__item__date">{item.date}</p>
                            <p className="timeline__item__text">{item.text}</p>
                        </li>
                    ))}
                </>
                : <LoadingComponent />
            }
        </ul>
    )
}

import React from "react";
import cx from "classnames/bind";

const Section = ({ children, subdued, title, maxHeight, scrollable, visible }) => {
  return (
    <div
      style={{ maxHeight }}
      className={cx({
        card__section: true,
        "card__section--scrollable": scrollable,
        "card__section--subdued": subdued,
        "u-hidden" :  visible === false
      })}
    >
      {title && (
        <div className="card__section__header">
          <h4>{title}</h4>
        </div>
      )}
      {children}
    </div>
  );
};

export default Section;

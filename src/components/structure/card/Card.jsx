import React from "react";
import { Link } from "react-router-dom";
import "./card.scss";
import Section from "./sub_components/Section";
import cx from 'classnames/bind'
const Card = ({
  children,
  title,
  sectioned,
  actions,
  primaryFooterAction,
  secondaryFooterActions,
}) => {
  return (
    <div className="card">
      {title && (
        <div className="card__header">
          <h2>{title}</h2>
          {actions && (
            <div className="card__header__actions">
              {actions.map((action, index) => (
                <span key={index} style={{margin: 0}} className={cx({
                  "tag tag--info": action.tag
                })} onClick={action.onAction}>{action.content}</span>
              ))}
            </div>
          )}
        </div>
      )}
      {sectioned ? <div className="card__section">{children}</div> : children}
      {primaryFooterAction && (
        <div className="card__footer">
          {secondaryFooterActions && (
            <button className="btn">{secondaryFooterActions.content}</button>
          )}
          <Link to={primaryFooterAction.url} className="btn btn--primary">
            {primaryFooterAction.content}
          </Link>
        </div>
      )}
    </div>
  );
};

Card.Section = Section;

export default Card;

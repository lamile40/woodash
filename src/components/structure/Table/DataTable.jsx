import React, { useState, useEffect } from 'react'
import './style.scss'
// import cx from 'classnames/bind'
import SkeletonDisplayText from '../ActivityIndicator/SkeletonDisplayText'

export const LoadingComponent = ({ lineLoading }) => {
    return (
        <tbody>
            {lineLoading.map((line, index) => (
                <tr key={index} >
                    {
                        line.map((content, key) => (
                            <td key={key} className={`data-table__cell`}>
                                <SkeletonDisplayText size={content} />
                            </td>
                        ))}
                </tr>
            ))}

        </tbody>
    )
}

const DataTable = ({ columnContentTypes, headings, rows, loading, fetching, footerContent }) => {

    return (
        <table className="data-table">
            {headings &&
                <thead>
                    <tr>
                        {headings.map((heading, index) => (
                            <th key={index}  scope="col" className={`data-table__cell data-table__cell--${columnContentTypes[index]}`}>{heading}</th>
                        ))}
                    </tr>
                </thead>
            }
            {fetching ? <LoadingComponent lineLoading={loading} /> :
                rows &&
                <tbody>
                    {rows.map((row, index) => (
                        <tr key={index}  >
                            {
                                row.map((content, index1) => (
                                    <td key={index1}  scope="row" className={`data-table__cell data-table__cell--${columnContentTypes[index]}`}><span>{content}</span></td>
                                ))}
                        </tr>
                    ))}
                </tbody>
            }
        </table>
    )
}

export default DataTable

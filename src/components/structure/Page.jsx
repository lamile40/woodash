import { Link } from "react-router-dom";
import React from "react";

const Page = ({
  children,
  title,
  breadcrumbs,
  primaryAction,
  secondaryActions,
}) => {
  return (
    <div className="page">
      {breadcrumbs && (
        <div className="page__breadcrumb">
          <Link to={breadcrumbs.url} className="page__breadcrumb__icon">
            <span className="app-icon">
              <svg
                viewBox="0 0 20 20"
                className="Polaris-Icon__Svg"
                focusable="false"
                aria-hidden="true"
              >
                <path
                  d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16"
                  fillRule="evenodd"
                ></path>
              </svg>
            </span>
            {breadcrumbs.content}
          </Link>
        </div>
      )}
      <div className="page__header">
        <div className="page__header__title">
          <h1>{title}</h1>
          {secondaryActions &&
            secondaryActions.map((action) => ( action ?
              action.url ? <a target="_blank" rel="noopener noreferrer" href={action.url} className="btn btn--outline">
                {action.content}
              </a> : <button onClick={action.onAction} className="btn btn--outline">
                  {action.content}
              </button> : null
            ))}
        </div>
        {primaryAction && (
          <div className="page__header__primary-action">
            <Link to={primaryAction.url} className="btn btn--primary">
              {primaryAction.content}
            </Link>
          </div>
        )}
      </div>
      <div className="page__content">{children}</div>
    </div>
  );
};

export default Page;

import React from 'react'
import cx from 'classnames/bind'
import './style.scss'
const SkeletonDisplayText = ({ size, lines, fullW }) => {
    return (
        lines ?
            <>
                <div className={cx({
                    "skeleton-display-text": true,
                    "skeleton-display-text--large": size === 'large',
                    "skeleton-display-text--mini": size === 'mini',
                    "skeleton-display-text--fullw": fullW
                })} />
                <div className={cx({
                    "skeleton-display-text": true,
                    "skeleton-display-text--large": size === 'large',
                    "skeleton-display-text--mini": size === 'mini',
                    "skeleton-display-text--fullw": fullW
                })} />
            </>
            : <div className={cx({
                "skeleton-display-text": true,
                "skeleton-display-text--large": size === 'large',
                "skeleton-display-text--mini": size === 'mini',
                "skeleton-display-text--fullw": fullW
            })} />
    )
}

export default SkeletonDisplayText

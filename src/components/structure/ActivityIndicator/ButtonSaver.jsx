import React from 'react'
import cx from 'classnames/bind'
import { Spinner } from '@shopify/polaris'

const ButtonSaver = ({content, type, processing, onAction}) => {
    return (
        <button onClick={onAction} className={cx({
            btn: true,
            "btn--primary" : type === 'primary',
            "btn--secondary" : type === 'secondary'
        })}>
            {!processing ? content : <Spinner accessibilityLabel="Spinner example" size="small" color="white" />}
        </button>

    )
}

export default ButtonSaver

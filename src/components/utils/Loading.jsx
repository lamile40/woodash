import React from 'react';

export const AppLoader = (props) => {
    return(
        <div className="wrapper" style={{
            height: '100vh'
        }}>
            <h1>Loading {props.title}...</h1>
        </div>
    )
}
import React from "react";
import moment from "moment";
import "moment/locale/fr"; // without this line it didn't work

export const Date = (props) => {
  return (
    <span>
      {moment(props.date).format(props.format)} à{" "}
      {moment(props.date).format(props.hours)}
    </span>
  );
};

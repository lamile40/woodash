import { Thumbnail } from '@shopify/polaris'
import React from 'react'

const ProcessUploadFile = ({file, api, onImageAdded, index}) => {
    
    React.useEffect(() => {
        
        var reader = new FileReader();
        reader.onload = function (e) {
            const img = e.target.result;
            api.ProductService.uploadMedias({
                title : file.name,
                media_attachment : img
            }).then(data => onImageAdded(data))
        };

        reader.readAsDataURL(file);  

        return () => {}
    }, [])

    return (
        <Thumbnail
            size="large"
            alt={file.name}
            source={window.URL.createObjectURL(file)}
       /> 
    )
}

export default ProcessUploadFile

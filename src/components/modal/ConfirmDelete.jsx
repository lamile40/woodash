import React, {useEffect} from 'react'
import { ModalContext } from '../../context/ModalContext';

const ConfirmDelete = ({header, toDelete, onConfirm}) => {

    const [modal, setContextModal] = React.useContext(ModalContext);

    return (
        <>
            <div className="modal__content">
                {header}
            </div>
            <div className="modal__footer u-flex u-flex--center-v-end">
                <button className="btn btn--seconday"  onClick={ () => setContextModal({...modal, display: false}) }>Cancel</button>
                <button className="btn btn--danger" onClick={ () => onConfirm(toDelete) }>Delete</button>
            </div>
        </>
    )
}

export default ConfirmDelete

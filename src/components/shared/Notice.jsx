import React from 'react'
import cx from 'classnames/bind'

export const Notice = ({type, content, style}) => {
    return (
        <div style={style} className={cx({
            notice: true,
            'notice--warning': type === 'warning'
        })}>
            {content}
        </div>
    )
}

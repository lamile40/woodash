import React from "react";
import cx from "classnames/bind";
import { ModalContext } from "../../context/ModalContext";

function Modal() {
  const [modalData, setModalData] = React.useContext(ModalContext);

  React.useEffect(() => {
      console.log(modalData)
      return () => {
          
      }
  }, [])

  return (
    <>
      <div
        className={cx({ backdrop: true })}
        onClick={() =>
          setModalData({ ...modalData, display: !modalData.display })
        }
      />
      <div className={cx({ modal: true, show: modalData.display })}>
        {
        modalData.props.header && (
            <div className="modal__header">
            {modalData.props.header }
            </div>
        )}
        { modalData.props.sectionned ?
            <>
                <div className="modal__content">
                {modalData.component}
                </div>
            
                { modalData.props.footer && (
                    <div className="modal__footer">
                    {modalData.props.footer}
                    </div>
                )}
            </>
            : modalData.component }
      </div>
    </>
  );
}

export default Modal;

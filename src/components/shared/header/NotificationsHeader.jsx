import React from "react";

export default function NotificationsHeader() {
  return (
    <div className="notification">
      <i className="far fa-bell"></i>
      <span className="notification__badge">12</span>
    </div>
  );
}

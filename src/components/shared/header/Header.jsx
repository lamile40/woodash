import React from "react";
import NotificationsHeader from "./NotificationsHeader";
function Header() {
  return (
    <header className="header">
      <div className="header__logo">
        <h1>WooDash</h1>
      </div>
      <div className="header__infos">
        <div className="header__page-active">Dashboard</div>
        <div className="header__actions">
          <NotificationsHeader />
      </div>
      </div>
    </header>
  );
}

export default Header;

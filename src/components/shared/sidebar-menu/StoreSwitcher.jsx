import React, {useEffect} from 'react'
import {connect} from 'react-redux'
import { switchStore } from '../../../stores/actions';

import { appSelector } from '../../../stores/app/app.selectors';

const StoreSwitcher = (props) => {

    const openStoreSelectorModal = () => {
        alert('TODO store selector')
    }

    return (
        <div className="store-switch" onClick={ openStoreSelectorModal }>
            {props.app.default_store}
            <i className="fas fa-sort-down" />
        </div>
    )
}




export default 
    connect((state) => ({
      app: appSelector(state),
    }),
    {switchStore}
)(StoreSwitcher);
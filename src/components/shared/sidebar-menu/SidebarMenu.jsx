import React from "react";
import SettingsIcon from "../../../assets/images/SettingsIcon.svg";
import { routes } from "../../../routing/routes";
import { NavLink } from "react-router-dom";
import StoreSwitcher from "./StoreSwitcher";

const SidebarMenu = (props) => {
  return (
    <div className="sidebar-menu">
      <span>
        <div className="sidebar-menu__links">
          <ul className="links">
            {routes.map(
              (r, index) =>
                r.routeProps &&
                r.routeProps.main_nav !== undefined && (
                  <li key={index}>
                    <NavLink
                      activeClassName="links--active"
                      key={r.path}
                      className="navigation__link"
                      to={r.path}
                    >
                      {r.routeProps.icon && (
                        <i className={r.routeProps.icon}></i>
                      )}
                      {r.routeProps.main_nav_label}
                    </NavLink>
                    {r.sub_menu && (
                      <ul className="links__sub">
                        {r.sub_menu.map((sub_link, key) => (
                          <li key={key}>
                            <NavLink to={sub_link.path}>
                              {sub_link.label}
                            </NavLink>
                          </li>
                        ))}
                      </ul>
                    )}
                  </li>
                )
            )}
          </ul>
        </div>
      </span>
      <div className="sidebar-menu__account">
        <div className="current-store">
          <StoreSwitcher />
          <span>Vous etes actuellement connecté sur cette boutique</span>
          {props.default_store && <a
            href={props.default_store.url}
            target="_blank"
            rel="noopener noreferrer"
          >
            Afficher ma boutique
          </a> }
        </div>
        <NavLink to="/settings" className="my-account-link">
          <img src={SettingsIcon} alt="" />
          Settings
        </NavLink>
      </div>
    </div>
  );
};

export default SidebarMenu;

import { TextField } from "@shopify/polaris";
import React from "react";

const InputElement = (props) => {
  return (
    <div className="form-element form-element--input">
      <TextField label={props.label} helpText={props.helpText} />
    </div>
  );
};

export default InputElement;

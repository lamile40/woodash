import { TextField } from "@shopify/polaris";
import React, { useCallback, useState } from "react";

const FilterList = (props) => {
  const [textFieldValue, setTextFieldValue] = useState("");

  const handleTextFieldChange = useCallback(
    (value) => setTextFieldValue(value),
    []
  );

  const handleClearButtonClick = useCallback(() => setTextFieldValue(""), []);

  return (
    <div className="list-filters">
      <TextField
        placeholder={props.placeholder}
        value={textFieldValue}
        onChange={handleTextFieldChange}
        clearButton
        onClearButtonClick={handleClearButtonClick}
      />
    </div>
  );
};

export default FilterList;

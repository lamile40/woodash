import React from "react";

const ListingContent = (props) => {
  return <tbody className="listing-table__content">{props.children}</tbody>;
};

export default ListingContent;

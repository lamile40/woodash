import React from "react";

const HeadingList = (props) => {
  return (
    <thead className="listing-table__header">
      <tr>
        {props.entries.map((entry, index) => (
          <th key={index}>{entry}</th>
        ))}
      </tr>
    </thead>
  );
};

export default HeadingList;

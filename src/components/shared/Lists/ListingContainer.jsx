import React from "react";

const ListingContainer = (props) => {
  return <table className="listing-table">{props.children}</table>;
};

export default ListingContainer;

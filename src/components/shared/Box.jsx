import React from "react";
import IconHelp from "../../assets/images/IconHelp.svg";
const Box = (props) => {
  return (
    <div className={`box box--${props.size}`}>
      <header className="box__header">
        <span>
          {props.title}
          <img src={IconHelp} alt="help" />
        </span>
      </header>
      <div className="box__content">{props.children}</div>
    </div>
  );
};

export default Box;

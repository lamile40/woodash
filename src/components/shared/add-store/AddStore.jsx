import React, {useState, useEffect} from "react";
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import cx from "classnames/bind";
import { connect } from "react-redux";
import { addStore } from "../../../stores/actions";
import { Redirect } from "react-router-dom";
import LocalStorageInstance from "../../../helpers/LocalStorage";

const ComposantErreur = props => {
  return (
    <div className="error-message">
      <span>{props.children}</span>
    </div>
  );
};

const ComposantInput = ({ field, form: { touched, errors }, ...props }) => {
  return (
    <div className={cx("form-group", { invalid: errors[field.name] })}>
      <label> {props.label} </label>
      <input type="text" {...props} className="form-control" {...field} />
    </div>
  );
};


const AddStore = (props) => {

    const [store, setStore] = useState(null)

    const submit = (values, actions) => {
        setStore({...values, provider: 'woocommerce'})
        
    };

    useEffect(() => {
        if(store !== null) {
            props.addStore([
              {
                name: "Wc SHOP demo",
                url: "https://wcshop.local/",
                consumerKey: "ck_46cce74918a12757b92719cce64c3f3082947fbe",
                consumerSecret: "cs_8405228ec6a131eefd3019456fea62b1c337adb9",
                provider: "woocommerce",
              },
              {
                name: "Willo",
                url: "https://willow.local/",
                consumerKey: "ck_0012ecc5651609890ea697cfee4f3e2528617216",
                consumerSecret: "cs_ecbef06e53c456f5f89b85d76b323d217de0e51b",
                provider: "woocommerce",
              },
            ])
            LocalStorageInstance.store('stores', JSON.stringify([
              {
                name: "Wc SHOP demo",
                url: "https://wcshop.local/",
                consumerKey: "ck_46cce74918a12757b92719cce64c3f3082947fbe",
                consumerSecret: "cs_8405228ec6a131eefd3019456fea62b1c337adb9",
                provider: "woocommerce",
              },
              {
                name: "Willo",
                url: "https://willow.local/",
                consumerKey: "ck_0012ecc5651609890ea697cfee4f3e2528617216",
                consumerSecret: "cs_ecbef06e53c456f5f89b85d76b323d217de0e51b",
                provider: "woocommerce",
              },
            ]))
            LocalStorageInstance.store('default_store', 'Wc SHOP demo')
        }
        return () => {}
    }, [store])
    
    const schema = Yup.object().shape({
        name: Yup.string()
            .required("Champs obligatoire"),
        url: Yup.string().required("Champs obligatoire"),
        consumerKey: Yup.string().required("Champs obligatoire"),
        consumerSecret: Yup.string().required("Champs obligatoire"),
    });

    return (
        <>
        {store && <Redirect to='/dashboard' />}
        <Formik
            onSubmit={submit}
            initialValues={{ name: "Demo store", url: "https://wcshop.local/", consumerKey: 'ck_46cce74918a12757b92719cce64c3f3082947fbe', consumerSecret: 'cs_8405228ec6a131eefd3019456fea62b1c337adb9'}}
            validationSchema={schema}
            validateOnChange={true}
            >
            {({ handleSubmit, isSubmitting }) => (
                <form
                onSubmit={handleSubmit}
                className="w-50 bg-white border p-5 d-flex flex-column"
                >
                <Field name="name" label="Store name" component={ComposantInput} />
                <ErrorMessage name="name" component={ComposantErreur} />

                <Field name="url" label="Store URL" component={ComposantInput} />
                <ErrorMessage name="url" component={ComposantErreur} />

                <Field name="consumerKey" label="Consumer KEY" component={ComposantInput} />
                <ErrorMessage name="consumerKey" component={ComposantErreur} />

                <Field name="consumerSecret" label="Consumer SECRET" component={ComposantInput} />
                <ErrorMessage name="consumerSecret" component={ComposantErreur} />
                

                <button
                    type="submit"
                    className="btn btn-primary"
                    disabled={isSubmitting}
                >
                    Connect store
                </button>
                </form>
            )}
        </Formik>
        </>
    )
}


export default connect(
    () => ({}),
    {addStore}
)(AddStore)


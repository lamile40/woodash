import { createSelector } from 'reselect';

export const postsSelector = state => state.posts;

export const postsIsLoadingSelector = createSelector(
    [ postsSelector ],
    posts => posts.isLoading
)

export const postsListSelector = createSelector(
    [postsSelector],
    posts => posts.list
)

export const postsSingleSelector = createSelector(
    [postsSelector],
    posts => posts.single
)
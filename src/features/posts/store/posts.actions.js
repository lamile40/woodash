export const REQUEST_POSTS = 'REQUEST_POSTS';
export const FETCH_POSTS  = 'FETCH_POSTS';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_ERROR = 'FETCH_POSTS_ERROR';

// ASYNC ACTIONS
export const TRY_ADD_POST = 'TRY_ADD_POST';


export const requestPosts = () => ({
    type: REQUEST_POSTS,
});

export const fetchPostsSucess = posts => ({
    type: FETCH_POSTS_SUCCESS,
    posts
});

export const fetchPostsError = error => ({
    type: FETCH_POSTS_ERROR,
    error
});

export const resetPosts= () => async dispatch => {
  dispatch( fetchPostsSucess([]) );
};

export const fetchPosts = () => async dispatch => {
    dispatch( requestPosts() )  
    dispatch( fetchPostsSucess([]) );
};

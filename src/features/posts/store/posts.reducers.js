import initialState from "./initialState";
import * as actions from './posts.actions';

export default (state = initialState, action) => {
    switch (action.type) {
        case actions.REQUEST_POSTS: {
            return {
                ...state,
                isLoading: true
            }
        }
        case actions.FETCH_POSTS_SUCCESS: {
            return {
                ...state,
                isLoading: false,
                error: null,
                lists: action.posts
            }
        }
        case actions.FETCH_POSTS_ERROR: {
            return {
                ...state,
                isLoading: false,
                error: action.error,
            }
        }
        default: {
            return state
        }
    }
}
import { injectReducer } from "../../../stores";
import postsReducers from "./posts.reducers";

injectReducer('posts', postsReducers)
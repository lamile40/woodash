import React from 'react'
import Loadable from 'react-loadable';
import { AppLoader } from '../../components/utils/Loading';

const LazyPosts = Loadable({
  loader: () => import(/* webpackChunkName: 'posts' */ './index'),
  loading: () => <AppLoader title='todos' />
})

export default LazyPosts;
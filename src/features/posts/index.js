import React, { useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  postsIsLoadingSelector,
  postsListSelector
} from "./store/posts.selectors";
import { fetchPosts } from "./store/posts.actions";
import "./store/index";

function index(props) {
  useEffect(() => {
    // Redux action
    props.fetchPosts();

    return () => {};
  }, []);

  return !props.isLoading ? (
    <div>This is the post lists features INDEX</div>
  ) : null;
}

export default withRouter(
  connect(
    state => ({
      isLoading: postsIsLoadingSelector(state),
      posts: postsListSelector(state)
    }),
    {
      fetchPosts
    }
  )(index)
);

import React from "react";
import Card from "../../components/structure/card/Card";
import Layout from "../../components/structure/layout/Layout";
import Page from "../../components/structure/Page";

const ProductLoading = () => {
  return (
    <Page fullWidth>
      <Layout>
        <Layout.Section>
          <Card sectioned></Card>
        </Layout.Section>
        <Layout.Section secondary></Layout.Section>
      </Layout>
    </Page>
  );
};

export default ProductLoading;

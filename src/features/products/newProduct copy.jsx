import React, { useState, useCallback } from "react";
import {
  Layout,
  Card,
  Page,
  DropZone,
  Stack,
  Thumbnail,
  Caption,
  Checkbox,
  FormLayout,
  TextField,
} from "@shopify/polaris";
import InputElement from "../../components/shared/form/InputElement";

const newProduct = () => {
  // const [product, setProduct] = useState({});
  const [files, setFiles] = useState([]);
  const [needShipment, setNeedShipment] = useState(true);
  const [hasVariants, setHasVariants] = useState(false);

  const handleDropZoneDrop = useCallback(
    (_dropFiles, acceptedFiles, _rejectedFiles) =>
      setFiles((files) => [...files, ...acceptedFiles]),
    []
  );

  const validImageTypes = ["image/gif", "image/jpeg", "image/png"];

  const fileUpload = !files.length && <DropZone.FileUpload />;
  const uploadedFiles = files.length > 0 && (
    <Stack vertical>
      {files.map((file, index) => (
        <Stack alignment="center" key={index}>
          <Thumbnail
            size="small"
            alt={file.name}
            source={
              validImageTypes.indexOf(file.type) > 0
                ? window.URL.createObjectURL(file)
                : "https://cdn.shopify.com/s/files/1/0757/9955/files/New_Post.png?12678548500147524304"
            }
          />
          <div>
            {file.name} <Caption>{file.size} bytes</Caption>
          </div>
        </Stack>
      ))}
    </Stack>
  );

  return (
    <div className="new-product">
      <Page
        fullWidth
        title="New product"
        breadcrumbs={[{ content: "Products", url: "/products" }]}
        secondaryActions={[
          { content: "Duplicate" },
          { content: "View on your store" },
        ]}
      >
        <Layout>
          <Layout.Section>
            <Card sectioned>
              <InputElement label={"Title"} id={"product_title"} />
              <InputElement label={"Description"} id={"product_description"} />
            </Card>

            <Card title="Medias gallery" sectioned>
              <DropZone onDrop={handleDropZoneDrop}>
                {uploadedFiles}
                {fileUpload}
              </DropZone>
            </Card>

            <Card title="Inventory" sectioned>
              <InputElement id={"product_title"} label="SKU" />
            </Card>

            <Card title="Shipment">
              <Card.Section>
                <Checkbox
                  label="This product must be shipped"
                  checked={needShipment}
                  onChange={() => setNeedShipment(!needShipment)}
                />
              </Card.Section>
              {needShipment && (
                <Card.Section title="Weight (Kg)">
                  <InputElement
                    id={"product_title"}
                    helpText="Weight in decimal form
"
                  />
                  <FormLayout>
                    <FormLayout.Group condensed>
                      <TextField label="Length" onChange={() => {}} />
                      <TextField label="Width" onChange={() => {}} />
                      <TextField label="Height" onChange={() => {}} />
                    </FormLayout.Group>
                  </FormLayout>
                </Card.Section>
              )}
            </Card>

            <Card title="Variants">
              <Card.Section>
                <Checkbox
                  label="
                  This product has several options, p. ex. different sizes or colors."
                  checked={hasVariants}
                  onChange={() => setHasVariants(!hasVariants)}
                />
              </Card.Section>
              {hasVariants && <Card.Section title="Variants"></Card.Section>}
            </Card>
          </Layout.Section>
          <Layout.Section secondary>
            <Card title="Product availibility" sectioned></Card>
            <Card title="Categories" sectioned></Card>
            <Card title="Tags" sectioned></Card>
          </Layout.Section>
        </Layout>
      </Page>
    </div>
  );
};

export default newProduct;

import React from 'react'
import { useParams } from 'react-router-dom';
import ButtonSaver from '../../../components/structure/ActivityIndicator/ButtonSaver';
import Card from '../../../components/structure/card/Card';
import FormLayout from '../../../components/structure/Forms/form-layout/FormLayout';
import InputElement from '../../../components/structure/Forms/InputElement';
import Select from '../../../components/structure/Forms/Select';

const VariantProductItem = ({product, updateProductVariant ,index, api}) => {
    const [variantTitle, setVariantTitle] = React.useState('')
    const [editMode, setEditMode] = React.useState(false)
    const [saving, setSaving] = React.useState(false)
    const [selected, setSelected] = React.useState('instock');

    let { id } = useParams()
    React.useEffect(() => {
        const title = product.attributes.reduce((acc, item) =>{
            acc += `${item.option.toUpperCase()} / `
            return acc;
        }, '');

        setVariantTitle(title.slice(0, -3))

        console.log('update')

        return () => {}
    }, [])

    const save = () => {

        setSaving(true)

        if( product.image === null ) {
            delete product['image']; 
        }
        api.ProductService.updateProductVariation(id, product).then( () => setSaving(false))
    }

    const options = [
        {label: 'In stock', value: 'instock'},
        {label: 'Out of stock', value: 'outofstock'},
        {label: 'On backorder', value: 'onbackorder'},
      ];


    return (
        <div className="variations-list__item variant-item ">
            <header className="u-flex u-flex--sb" onClick={() =>setEditMode(!editMode)} >
            <div className="variant-item__title">
                {product.image ?
                <img src={ product.image.src} alt=""/> : <svg viewBox="0 0 20 20" class="_1nZTW _2QX6Y"><path d="M2.5 1A1.5 1.5 0 0 0 1 2.5v15A1.5 1.5 0 0 0 2.5 19h15a1.5 1.5 0 0 0 1.5-1.5v-15A1.5 1.5 0 0 0 17.5 1h-15zm5 3.5c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zM16.499 17H3.497c-.41 0-.64-.46-.4-.79l3.553-4.051c.19-.21.52-.21.72-.01L9 14l3.06-4.781a.5.5 0 0 1 .84.02l4.039 7.011c.18.34-.06.75-.44.75z"></path></svg>}
                <span>
                    {variantTitle}
                    {product.regular_price === '' && <span>Not visible in store, set a price first</span> }
                </span>
            </div>
            <button className="btn btn--secondary">
                Edit
            </button>
            </header>
            {editMode &&
            <div className="variant-item__form">
                <FormLayout>
                    <FormLayout.Group>
                        <InputElement 
                            value={product.regular_price} 
                            onChange={ value => updateProductVariant('regular_price',value, index)} 
                            label="Price"
                        ></InputElement>
                        <InputElement 
                            value={product.sale_price} 
                            onChange={ value => updateProductVariant('sale_price',value, index)} 
                            label="Sale price"
                        ></InputElement>
                    </FormLayout.Group>
                    <Card title="Inventory">
                    <Card.Section>
                        <FormLayout>
                            <FormLayout.Group>
                                <InputElement 
                                    value={product.sku} 
                                    id={"product-sku"} 
                                    label={"SKU"} 
                                    onChange={ value => updateProductVariant('sku',value, index)} 
                                    />
                                <Select 
                                    label="Stock status"
                                    name="stock-status"
                                    options={options}
                                    onChange={ e => updateProductVariant('stock_status',e.target.value, index)} 
                                    value={product.stock_status ? product.stock_status :selected}
                                />
                            </FormLayout.Group>
                        </FormLayout>
                   
                   </Card.Section>
                    </Card>
                    <ButtonSaver onAction={save} content="Save" type={'primary'} processing={saving} />
                </FormLayout>
            </div>
            }
        </div>
    )
}

export default VariantProductItem

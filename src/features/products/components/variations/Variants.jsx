import React from 'react'
import VariantBuilder from './VariantBuilder';
import './style.scss'
import UsePrevious from '../../../../hooks/UsePrevious';
const Variants = ({product, setProduct, onAddVariantFilter, api}) => {
    const [disableAdd, setDisableAdd] = React.useState(false)

    const [variants, setVariants] = React.useState(product.attributes || []);

    const [doUpdate, setDoUpdate] = React.useState(false)


    const prevVariants = UsePrevious(variants)

    const empty_variant = {
        "visible": false,
        "variation": true,
        name : '', 
        options : [],
    }

    React.useEffect(() => {

        if( prevVariants && prevVariants.length > variants.length ) {
            setDoUpdate(true)
        }

        if(variants) {
            if( Object.keys(variants).length >= 3) {
                setDisableAdd(true)
            } else {
                setDisableAdd(false)
            }
        }

        setProduct({
            ...product,
            attributes: variants 
        })

        return () => {}
    }, [variants])

    const deleteOption = index => {
        setVariants( variants.filter( (_,i) => index !== i ) )
    }

    const updateVariant = (key, value, index) => {
        setVariants(
            variants.map( (variant, i) => i === index ? 
                {...variant, [key]: key === 'options' ? [...variant.options, value] : value}
               : variant
            )
        )
    }

    const deletOptionItem = (option, index) =>{
        setVariants(
            variants.map( (variant, i) => i === index ? 
                { ...variant,
                    options: variant.options.filter( opt => opt !== option) } 
               : variant
            )
        )
    }

    return (
        <div className="variants">
            {variants && variants.map( (variant, key) => (
                <VariantBuilder 
                    onAddVariantFilter={ (option) => onAddVariantFilter(option)}
                    deleteOption={ (index) => deleteOption(index)} 
                    onDeletOptionItem={(option, i) => deletOptionItem(option ,i) }
                    key={key} index={key + 1} 
                    variant={variant} 
                    updateOption={ (key, value, i) => updateVariant(key, value ,i) }
                    setProduct={setProduct}
                    product={product}
                    api={api}
                    doUpdateProps={ doUpdate }
                />
            ))}
            {!disableAdd &&
            <div className="u-flex" style={{ marginTop: 8 }}>
                <button 
                    className="btn btn--seconday" 
                    onClick={() => setVariants([...variants , empty_variant ])}
                >New variant option</button>
            </div>
}
        </div>
    )
}

export default Variants

import React from 'react'
import { useParams } from 'react-router-dom'
import { connect } from "react-redux";
import { editingProductSelector } from '../../store/products.selectors';
import { ModalContext } from '../../../../context/ModalContext';
import ConfirmDelete from '../../../../components/modal/ConfirmDelete';

const VariantBuilder = ({
    doUpdateProps ,
    index, 
    variant, 
    deleteOption, 
    updateOption, 
    onDeletOptionItem, 
    onAddVariantFilter, 
    product, 
    api,
    editingProduct
}) => {

    const [modal, setContextModal] = React.useContext(ModalContext);


    const { id } = useParams();

    const optionsRef = React.useRef()

    const handleKeyDown = e => {
        if (e.key === 'Enter') {
            if( optionsRef.current.value !== '' && !variant.options.includes(optionsRef.current.value ) ) {
                updateOption('options', optionsRef.current.value, index - 1)
                optionsRef.current.value = ''
            } else {
                optionsRef.current.classList.add('u-error')
                setTimeout(() => {
                    optionsRef.current.classList.remove('u-error')
                }, 750);
            } 
        }
    }

    const handleBlur= e => {
        if( optionsRef.current.value !== '' && !variant.options.includes(optionsRef.current.value ) ) {
            updateOption('options', optionsRef.current.value, index - 1)
            optionsRef.current.value = ''
        } else {
            optionsRef.current.classList.add('u-error')
            setTimeout(() => {
                optionsRef.current.classList.remove('u-error')
            }, 750);
        }
    }

    const confirmModal = (toDelete, option, index)=> {
        setContextModal({
            ...ModalContext,
            display: true,
            component: <ConfirmDelete header='test' toDelete={toDelete} onConfirm={ toDelete => {
                const batchDelete = {
                    delete: toDelete.reduce( (acc,item) => { acc.push(item.id); return acc; }, [])
                };
                api.ProductService.batchDeleteVariants(product, batchDelete).then( () =>{
                    onDeletOptionItem( option, index)
                    setContextModal({...modal, display: false})
                })
            }} />,
            props: {
                header : 'Are you sure?',
                sectionned: false
            }
        })
    } 

    React.useEffect(() => {
        if( doUpdateProps ) {

            api.ProductService.edit(product.id, product).then( () => {
                const batchCreate = {
                    update: []
                };
        
                  let final = [];
        
                  product.attributes.map(item => {
                      const test = item.options.reduce((acc,opt) =>{
                        acc.push( {'name': item.name, 'option': opt} )
                        return acc;
                      }, [])
                      final = [...final, test]
                })
            
                const result = final.reduce((a, b) => a.reduce((r, v) => r.concat(b.map(w => [].concat(v, w))), []));
        
                api.ProductService.getProductVariations(product.id).then(data => {
                    
                    const variation_ids = data.reduce( (acc,item) => { acc.push(item.id); return acc; }, [])
    
                    result.map( (variant, index) =>{
                        const id = variation_ids[index]
                        batchCreate.update[index] = {
                            id,
                            attributes : variant.length === undefined ? [variant] : variant
                        }
                    })

    
                    api.ProductService.batchCreateVariants(product, batchCreate)
                })
            })    


        }
        
        return () => {
            
        }
    }, [product])

    const deleteOptionItem = (option, index) => {

        const saved_attrs = editingProduct.attributes.filter( item => item.name === variant.name ).shift()
 
        if( id !== undefined  ) {
            if( !saved_attrs.options.includes(option) ) {
                onDeletOptionItem( option, index)
            } else   {

                api.ProductService.getProductVariations(product.id).then(data => {

                    const toDelete = data.filter( variation => {
                        const options = variation.attributes.reduce( (acc, attr) => {
                            acc.push(attr.option);
                            return acc;
                        }, [])

                        return options.includes(option)
                    })

                    confirmModal(toDelete, option, index)
                });
            }
                
        } else {
            onDeletOptionItem( option, index)
        }
    }

    return (
        <div className="variants__item" style={{ marginTop: 8 }}>
            <div className="variants__item__title u-flex u-flex--sb">
                Option {index}
                <button style={{color: 'red'}} className="btn btn--outline" onClick={ () => deleteOption( index - 1)} >Delete</button>
            </div>
            <div style={{flexDirection: 'row',marginTop: 8}} className="u-flex u-flex--wrap u-flex--sb">
                <div className="form-element form-element--input" style={{ flex: '25%' , marginBottom: 0}}>
                    <input onChange={ e => updateOption('name', e.target.value, index - 1) } type="text" name="variant_name" className="" value={variant.name}/>
                </div>
                <div style={{ 
                    flex: 'calc(75% - 16px)', 
                    marginLeft: 16, 
                    border: '2px solid #e0e0e0', 
                    borderRadius : '4px',
                    position: 'relative',
                }}>
                    {variant.options.length > 0 &&
                    <div className="u-flex u-flex--wrap" style={{ paddingTop: 8, paddingBottom: 4 }}>
                        { variant.options.map( option => <span onClick={() => onAddVariantFilter(option) } class="tag" style={{paddingRight: 35, cursor: 'pointer' }}>{option}<button onClick={() => deleteOptionItem(option,index - 1)}><i className="fas fa-trash-alt"/></button></span>) }
                    </div>}
                    <input 
                        style={{
                            bottom: 0,
                            left: 0,
                            border: 'none',
                            width: '100%',
                            padding: 12,
                            outline: 'none'
                        }}
                        onKeyDown={handleKeyDown}
                        onBlur={handleBlur}
                        ref={optionsRef}
                        type="text" 
                        name="variant_options"
                        className="hidden"
                        placeholder="Nom de l'option, tapez sur entrée pour ajouter"
                    />
                </div>
            </div>
        </div>
    )
}
 

export default connect(
    (state) => ({
      editingProduct: editingProductSelector(state),
    }),
)(VariantBuilder);
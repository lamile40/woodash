import React, { useState, useCallback } from 'react'
import { DropZone, Stack, Thumbnail } from "@shopify/polaris";
import ProcessUploadFile from '../../../components/ProcessUploadFile';
import cx from 'classnames/bind';

function ProductGallery({ api, onImageAdded, product }) {
   
    const [files, setFiles] = useState(product.images || []);
    const [rejectedFiles, setRejectedFiles] = useState([]);
    const hasError = rejectedFiles.length > 0;
  
    const handleDrop = useCallback(
      (_droppedFiles, acceptedFiles, rejectedFiles) => {
        setFiles((files) => [...files, ...acceptedFiles]);
        setRejectedFiles(rejectedFiles);
      },
      [],
    );

    const setImageProduct = data => {
        onImageAdded(data)
    }

  
    const fileUpload = !files.length && <DropZone.FileUpload />;
    const uploadedFiles = files.length > 0 && (
      <div className="u-flex product-images"  wrap={false}>
        {files.map((file, index) => (
            file.type ? (
                <div 
                className={cx({
                    image : true,
                    "image--featured" : index === 0
                })}
                onClick={()=> alert()}
            >
            <ProcessUploadFile 
                index={index + 1} 
                product={product} 
                onImageAdded={ (data) => setImageProduct(data) } 
                key={index} file={file} api={api}
            /></div>): 
            <div 
                className={cx({
                    image : true,
                    "image--featured" : index === 0
                })}
                onClick={()=> alert()}
            >
                <Thumbnail
                    key={index}
                    size="large"
                    alt={file.name}
                    source={file.source_url || file.src}
                />
                 <div className="image__overlay"></div>
            </div>
        ))}
        <div>
            <DropZone  accept="image/*" type="image" dropOnPage onDrop={handleDrop}>
            <DropZone.FileUpload />
            </DropZone>
        </div>
      </div>
    );
  
    return (
     files.length > 0  ?  uploadedFiles  : 
      <DropZone dropOnPage accept="image/*" type="image" onDrop={handleDrop}>
        {uploadedFiles}
        {fileUpload}
      </DropZone> 
    );
  }

  export default ProductGallery

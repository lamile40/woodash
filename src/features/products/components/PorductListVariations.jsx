import React , {useEffect,useState} from 'react'
import VariantProductItem from './VariantProductItem'
import './index.scss'
import { Notice } from '../../../components/shared/Notice'
import ButtonSaver from '../../../components/structure/ActivityIndicator/ButtonSaver'

const PorductListVariations = ({api,product, filter}) => {

    const [variations, setvariations] = useState(null)
    const [processing, setProcessing] = useState(null)
    const [variationNoPrice, setVaritaionNoPrice] = useState(0)
    const [, setNeedUpdate] = useState(false)

    useEffect(() => {
        api.ProductService.getProductVariations(product.id).then(data => setvariations(data))
        return () => {}
    }, [])

    useEffect(() => {
        if( variations ) {
            const total = variations.reduce( (acc, item) =>{
                if( item.regular_price === '' ) {
                    acc++
                }
                return acc;
            }, 0)

            setVaritaionNoPrice(total)
        }
        return () => {}
    }, [variations])

    const createAllVariants = () => {

        setProcessing(true)

        const batchCreate = {
            create: []
        };

          let final = [];

          product.attributes.map(item => {
              const test = item.options.reduce((acc,opt) =>{
                acc.push( {'name': item.name, 'option': opt} )
                return acc;
              }, [])
              final = [...final, test]
        })

        // console.log(final)

        const result = final.reduce((a, b) => a.reduce((r, v) => r.concat(b.map(w => [].concat(v, w))), []));

        result.map( (variant, index) =>{
            batchCreate.create[index] = {
                attributes : variant.length === undefined ? [variant] : variant
            }
        })

        if( result.length >= variations.length || result.length <= variations.length) {
            api.ProductService.edit(product.id , product).then(() =>{
                api.ProductService.batchDeleteVariants(product, {
                    delete: product.variations
                }).then(() => {
                    api.ProductService.batchCreateVariants(product, batchCreate).then( data => {
                        console.log(data.create)
                        setvariations(data.create)
                        setProcessing(false)
                    })
                })
            })
            
        } else  if( variations.length === 0) {
            // alert('create')
            api.ProductService.batchCreateVariants(product, batchCreate).then( data => {
                setvariations(data.create)
                setProcessing(false)
            })
        }
        
    }

    const updateProductVariant = (key, value, index) => {
        setvariations(
            variations.map( (variant, i) => i === index ? 
                { ...variant,
                    [key]: value
                } 
               : variant
            )
        )
    }

    useEffect(() => {
        if( variations ) {
            let final = [];

            product.attributes.map(item => {
                const test = item.options.reduce((acc,opt) =>{
                  acc.push( {'name': item.name, 'option': opt} )
                  return acc;
                }, [])
                final = [...final, test]
          })
    
          // console.log(final)
    
          const result = final.reduce((a, b) => a.reduce((r, v) => r.concat(b.map(w => [].concat(v, w))), []));
    
          setNeedUpdate(result.length >= variations.length)
        }
       
    
      return () => {
        
        }
    }, [product])

    useEffect(() => {
        // if( variations ) {

        //     const search = filter.map((item) => item.toUpperCase() , [])

        //     const search_result = variations.filter( item => {

        //         const IN = item.attributes.reduce((acc,item)=>{
        //             acc.push(item.option.toUpperCase())
        //             return acc;
        //         }, [])

        //         console.log(search, IN)
                
        //         return search.some(el => IN.includes(el))
        //     })

        //     setvariations(search_result)
        // } 

        return () => {}
    }, [filter])

    return (
        <div class="variations-list">
            { setNeedUpdate && ( 
                <ButtonSaver 
                    content="Create / Update all variations" 
                    type="primary" 
                    onAction={createAllVariants} 
                    processing={processing} 
                />
            )}
            {variationNoPrice > 0 && (
                <Notice 
                    style={{ marginTop: 16 }}
                    type="warning" 
                    content={`${variationNoPrice} variations do not have prices. Variations (and their attributes) that do not have prices will not be shown in your store.`}
                />
            )}
            {variations && variations.map( (product, index) => (
                <VariantProductItem 
                    api={api}
                    index={index} 
                    setvariations={setvariations} 
                    variations={variations} 
                    product={product} 
                    updateProductVariant={(key, value, index) => updateProductVariant(key, value, index)}
                />
            ))}
        </div>
    )
}

export default PorductListVariations

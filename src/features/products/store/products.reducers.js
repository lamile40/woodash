import initialState from "./initialState";
import * as actions from "./products.actions";

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_PRODUCTS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        error: null,
        data: action.data,
      };
    }
    case actions.FETCH_EDIT_PRODUCT_SUCCESS: {
      return {
        ...state,
        product: action.data,
      };
    }
    default: {
      return state;
    }
  }
};

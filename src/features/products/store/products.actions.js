import { WooCommerce } from "../../../conf/WooCommerceApi";

export const REQUEST_PRODUCTS = "REQUEST_PRODUCTS";
export const FETCH_PRODUCTS = "FETCH_PRODUCTS";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_EDIT_PRODUCT_SUCCESS = "FETCH_EDIT_PRODUCT_SUCCESS";
export const FETCH_PRODUCTS_ERROR = "FETCH_PRODUCTS_ERROR";

// ASYNC ACTIONS
export const TRY_ADD_ORDER = "TRY_ADD_ORDER";

export const requestProducts = () => ({
  type: REQUEST_PRODUCTS,
});

export const fetchProductsSucess = (data) => ({
  type: FETCH_PRODUCTS_SUCCESS,
  data,
});

export const fetchProductsError = (error) => ({
  type: FETCH_PRODUCTS_ERROR,
  error,
});

export const setEditingProduct = ( data ) => ({
  type: FETCH_EDIT_PRODUCT_SUCCESS,
  data,
});

export const fetchProducts = (api) => async (dispatch) => {
  dispatch(requestProducts());
  api.ProductService.fetchProducts().then(data => dispatch(fetchProductsSucess(data)))
};



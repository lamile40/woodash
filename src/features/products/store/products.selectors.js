import { createSelector } from "reselect";

export const productsSelector = (state) => state.products;

export const productsIsLoadingSelector = createSelector(
  [productsSelector],
  (products) => products.isLoading
);

export const productsDataSelector = createSelector(
  [productsSelector],
  (products) => products.data
);

export const editingProductSelector = createSelector(
  [productsSelector],
  (products) => products.product
);



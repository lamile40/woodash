import { injectReducer } from "../../../stores";
import homeReducers from "./products.reducers";

injectReducer("products", homeReducers);

import React from "react";
import Loadable from "react-loadable";
import { AppLoader } from "../../components/utils/Loading";

const LazyProducts = Loadable({
  loader: () => import(/* webpackChunkName: 'products' */ "./index"),
  loading: () => <AppLoader title="products" />,
});

export default LazyProducts;

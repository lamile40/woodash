import React from "react";
import "./store/index";
import { useEffect } from "react";
import { connect } from "react-redux";
import {
  productsDataSelector,
  productsIsLoadingSelector,
} from "./store/products.selectors";
import { fetchProducts } from "./store/products.actions";
import ListingContainer from "../../components/shared/Lists/ListingContainer";
import HeadingList from "../../components/shared/Lists/HeadingList";
import { Link } from "react-router-dom";
import ListingContent from "../../components/shared/Lists/ListingContent";
import FilterList from "../../components/shared/Lists/FilterList";
import Page from "../../components/structure/Page";
import Layout from "../../components/structure/layout/Layout";
import { UseApi } from "../../hooks/UseApi";

const index = (props) => {

  const api = UseApi(props.store)

  useEffect(() => {
    props.fetchProducts(api);
    return () => {};
  }, [props]);

  return (
    <Page
      title="Products"
      primaryAction={{ content: "Create product", url: "/products/new" }}
      secondaryActions={[
        {
          content: "Export",
          onAction: () => alert("Export action"),
        },
        { content: "Import", onAction: () => alert("Import action") },
      ]}
    >
      <Layout>
        <Layout.Section>
          <FilterList placeholder="Filtrer les produits" />
          <ListingContainer>
            <HeadingList
              entries={["Product", "Inventory", "Price", "Actions"]}
            />
            {props.data && (
              <ListingContent>
                {props.data.map((product) => (
                  <tr key={product.id}>
                    <td>
                      <div className="listing-table__content__line">
                        {product.images.length > 0 ? (
                          <img src={product.images[0].src} alt="" />
                        ) : (
                          <img src="https://via.placeholder.com/80" alt="" />
                        )}

                        <Link to={`/products/${product.id}`}>
                          {product.name}
                        </Link>
                      </div>
                    </td>
                    <td>
                      {product.stock_quantity
                        ? product.stock_quantity
                        : "Stocks non suivis"}
                    </td>
                    <td>{product.regular_price}</td>
                    <td>test</td>
                  </tr>
                ))}
              </ListingContent>
            )}
          </ListingContainer>
        </Layout.Section>
      </Layout>
    </Page>
  );
};

export default connect(
  (state) => ({
    isLoading: productsIsLoadingSelector(state),
    data: productsDataSelector(state),
  }),
  {
    fetchProducts,
  }
)(index);

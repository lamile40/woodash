import React, { useState, useEffect } from "react";
import { Redirect, useLocation, useParams } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import ButtonSaver from "../../components/structure/ActivityIndicator/ButtonSaver";
import Card from "../../components/structure/card/Card";
import Checkbox from "../../components/structure/Forms/Checkbox";
import FormLayout from "../../components/structure/Forms/form-layout/FormLayout";
import InputElement from "../../components/structure/Forms/InputElement";
import RichEditor from "../../components/structure/Forms/RichEditor";
import Select from "../../components/structure/Forms/Select";
import Layout from "../../components/structure/layout/Layout";
import Page from "../../components/structure/Page";
import { StoreContext } from "../../context/StoreContext";
import { UseApi } from "../../hooks/UseApi";
import PorductListVariations from "./components/PorductListVariations";
import ProductGallery from "./components/ProductGallery";
import Variants from "./components/variations/Variants";
import ProductLoading from "./ProductLoading";
import { connect } from "react-redux";
import './store'
import { editingProductSelector } from "./store/products.selectors";
import { setEditingProduct } from "./store/products.actions";
import UsePrevious from "../../hooks/UsePrevious";
import CollectionTagger from "./components/CollectionTagger";

const newProduct = (props) => {
  const [store] = React.useContext(StoreContext);
  const [product, setProduct] = useState({
    shipping_required: true,
    status: 'draft',
    weight : '',
    catalog_visibility: 'visible',
    type : 'simple',
    manage_stock: false,
    images :[],
    dimensions : {
      length: '',
      width: '',
      height: '',
    }
  });

  const prevProduct = UsePrevious(product)
  
  const [redirect, setRedirect] = useState(null);
  const [edited, setEdited] = useState({edited: false, saved: false});
  // const [files, setFiles] = useState([]);
  const [needShipment, setNeedShipment] = useState(true);
  const [saving, setSaving] = useState(false);
  const [dataLoaded, setDataLoaded] = useState(false);
  const [filterState, setFilterState] = useState([]);
  const [loading, setLoading] = useState(false);
  const [stockManagement, setStockManagement] = useState(false);
  const [soldIndividually, setSoldIndividually] = useState(false);
  const [selected, setSelected] = useState('instock');
  const [allowBackOrders, setAllowBackOrders] = useState('no');

  const  { id } = useParams()
  const location = useLocation()
  const { addToast } = useToasts()

  const post_status = ['draft','pending','publish'];

  const [medias, setMedias] = useState([]);
  
  useEffect(() => {
    setProduct({
      ...product,
      images : medias 
    })
    return () => {
      
    }
  }, [medias])

  const setOtherStatus = () => {
    const currentIndex = post_status.indexOf(product.status);
    if( post_status[currentIndex + 1] ) {
      updateProductData('status', post_status[currentIndex + 1])
    } else {
      updateProductData('status', post_status[0])
    }
  }

  const api = UseApi(props.store)

  useEffect(() => {
    updateProductData(['shipping_required','virtual'], [needShipment,!needShipment])    
    return () => {}
  }, [needShipment])

  useEffect(() => {
    updateProductData('stock_status', selected)
    return () => {}
  }, [selected])

  useEffect(() => {
    updateProductData('sold_individually', soldIndividually)
    return () => {}
  }, [soldIndividually])

  useEffect(() => {
    updateProductData('backorders',allowBackOrders)
    return () => {}
  }, [allowBackOrders])

  useEffect(() => {
    updateProductData('manage_stock',stockManagement)
    return () => {}
  }, [stockManagement])
  
  useEffect(() => {
    if( id ) {
      setLoading(true)
      loadProduct(id)
    } else {
      props.setEditingProduct(product)
    }

    return () => {}
  }, [location])
  
  useEffect(() => {

    if(product.id) {
      setStockManagement(product.manage_stock)
      setAllowBackOrders(product.backorders)
      setSoldIndividually(product.sold_individually)
    }

    if( dataLoaded ) {
      setEdited({
        ...edited,
        edited:  JSON.stringify(props.editingProduct) !== JSON.stringify(product)
      })
    } else {
      console.log(props.editingProduct, product)
      setEdited({
        ...edited,
        edited:  JSON.stringify(props.editingProduct) !== JSON.stringify(product)
      })
    }
    
    return () => {}
    
  }, [product]);

  useEffect(() => {
    if( edited.saved ) {
      setEdited({
        edited: false,
        saved: false
      })
    }
    return () => {}
  }, [edited])
  
  useEffect(() => {

    window.onbeforeunload = unloadPage;

    return () => {
      
    }
  }, [edited])
  
  const options = [
    {label: 'In stock', value: 'instock'},
    {label: 'Out of stock', value: 'outofstock'},
    {label: 'On backorder', value: 'onbackorder'},
  ];

  const options_backorders = [
    {label: 'Do not allow', value: 'no'},
    {label: 'Allow, but notify customer', value: 'notify'},
    {label: 'Allow', value: 'yes'},
  ];

  const shop_visibility = [
    {label: 'Shop and search results', value: 'visible'},
    {label: 'Shop only', value: 'catalog'},
    {label: 'Search results only', value: 'search'},
    {label: 'Hidden', value: 'hidden'},
  ];

  const updateProductData = ( key, value ) => {
    if( typeof key == 'object') {
      Array.from(key).map((element, index) => {
        setProduct({
          ...product,
          [element] : value[index]
        })
      });
    }else {
      setProduct({
        ...product,
        [key] : value
      })
    }
   
  }

  const save = () => {

    setSaving(true)

    if(id) {
      api.ProductService.edit(id, product).then(() => {
        setEdited({...edited, saved: true});
        setSaving(false)
        addToast('Saved Successfully', { appearance: 'success' })
      }).catch(() =>{
        addToast('Error while saving product', { appearance: 'error' })

      })
    } else {
      api.ProductService.create(product).then(data =>{
        setRedirect(data.id)
        setSaving(false)
      })
    }
  }

  const loadProduct = (id) => {
    api.ProductService.get(id).then(data => {
      props.setEditingProduct(data)
      setProduct(data)
      setLoading(false)
      setMedias( data.images )
      setDataLoaded(true)
    })
  }

  const unloadPage = () => {
    if(edited.edited){
        return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
    }
  }

  return (
    <>
    { redirect !== null && <Redirect
      to={{
        pathname: `/products/${redirect}`,
      }}
    /> }

    {loading ? <ProductLoading /> : 
    <div className="new-product">
      { edited.edited  && <header className="header notice">
      <div className="header__logo"><h1>WooDash</h1></div>
        <div className="header__infos">
          Unsaved changes
          <div className="header__actions">
            <button className="btn btn--secondary" onClick={() => alert('Do cancel func')}>Cancel</button>
            <ButtonSaver onAction={save} content="Save" type={'primary'} processing={saving} />
          </div>
        </div>
      </header>
      }
      <Page
        fullWidth
        title={ Object.keys(product).length > 0 ? product.name : "New product"}
        breadcrumbs={{ content:"Products", url: "/products" }}
        secondaryActions={[
          product.id && {content: 'Duplicate', onAction: () => alert() },
          product.permalink && {content: 'View on store', url: product.permalink },
        ]}
      >
        <Layout>
          <Layout.Section>
            <Card sectioned>
              <InputElement value={product.name} id={"name"} label={"Title"} onChange={ value => updateProductData('name', value)}/>
              <RichEditor />
              {/* <InputElement label={"Description"} id={"product_description"} /> */}
            </Card>
            <Card title="Medias gallery" sectioned>
              <ProductGallery product={product} onImageAdded={ img => setMedias((medias) => [...medias,  img ]) } api={api} />
            </Card>
            <Card title="Pricing" sectioned>
              <FormLayout>
                <FormLayout.Group>
                  <InputElement value={product.regular_price} id={"regular_price"} label={"Price"} onChange={ value => updateProductData('regular_price', value)}/>
                  <InputElement value={product.sale_price} id={"sale_price"} label={"Sale price"} onChange={ value => updateProductData('sale_price', value)}/>
                </FormLayout.Group>
              </FormLayout>
            </Card>
            <Card title="Inventory">
              <Card.Section>
              <FormLayout>
                <FormLayout.Group>
                  <InputElement value={product.sku} id={"product-sku"} label={"SKU"} onChange={ value => updateProductData('sku', value)}/>
                  {(!stockManagement || !product.manage_stock) &&
                  <Select 
                    label="Stock status"
                    name="stock-status"
                    options={options}
                    onChange={(e) => setSelected(e.target.value)}
                    value={product.stock_status ? product.stock_status :selected}
                  />}
                </FormLayout.Group>
                </FormLayout>
              </Card.Section>
              <Card.Section>
                <Checkbox 
                  label="Enable stock management at product level" 
                  checked={product.manage_stock ? product.manage_stock : stockManagement} 
                  onChange={()=>setStockManagement(!stockManagement)}
                ></Checkbox>
                {(stockManagement || product.manage_stock) && <FormLayout>
                <FormLayout.Group>
                  <InputElement value={product.stock_quantity} id={"product-sku"} label={"Stock quantity "} onChange={ value => updateProductData('stock_quantity', value)}/>
                  <Select 
                    label="Allow backorders?"
                    name="allow-back-orders"
                    options={options_backorders}
                    onChange={(e) => setAllowBackOrders(e.target.value)}
                    value={product.backorders ? product.backorders : allowBackOrders}
                  />
                </FormLayout.Group>
                </FormLayout>}
              </Card.Section>

              <Card.Section>
              <Checkbox 
                label="Enable this to only allow one of this item to be bought in a single order" 
                checked={soldIndividually} 
                onChange={()=>setSoldIndividually(!soldIndividually)}
                ></Checkbox>

              </Card.Section>
            </Card>
            <Card title="Shipment">
              <Card.Section>
              <Checkbox
                id={'need_shipment'}
                checked={product.shipping_required }
                label="This product needs to be shipped"
                onChange={ (value) => {setProduct({...product, shipping_required: value, virtual: !value})}}

              />
              </Card.Section>
              <Card.Section visible={(Object.keys(product).length > 0 && product.shipping_required)}>
                <FormLayout>
                  <InputElement onChange={ value => updateProductData('weight', value)} value={product.weight} id={"weight"} label={"Weight (Kg)"} />
                  <FormLayout.Group>
                    <InputElement onChange={ value => setProduct({...product, dimensions:{...product.dimensions, length: value}})} value={product.dimensions.length} id={"regular_price"} label={"Length"}/>
                    <InputElement onChange={ value => setProduct({...product, dimensions:{...product.dimensions, width: value}})} value={product.dimensions.width} id={"sale_price"} label={"Width"}/>
                    <InputElement onChange={ value => setProduct({...product, dimensions:{...product.dimensions, height: value}})} value={product.dimensions.height} id={"sale_price"} label={"height"}/>
                  </FormLayout.Group>
                </FormLayout>
              </Card.Section>
              
              
            </Card>
            <Card title="Variants">
              <Card.Section>
                <Checkbox
                  id={'need_variants'}
                  label="This product has multiple options, like different sizes or colors"
                  checked={ product.type === 'variable' }
                  onChange={(variable) => updateProductData('type',  variable ? 'variable' : 'simple')}
                />
                
              </Card.Section>

              { product.type === 'variable'  && <><Card.Section title="Options" >
                <Variants 
                  onAddVariantFilter={ option => setFilterState([...filterState, option]) } 
                  product={product} 
                  setProduct={setProduct} 
                  api={api}
                />
                </Card.Section>
                { product.attributes && product.attributes.length > 0   && 
                <Card.Section title="Products" subdued>
                  <PorductListVariations api={api} product={product} filter={filterState}/>  
                </Card.Section>}</>
                }
            </Card>
          </Layout.Section>
          <Layout.Section secondary sticky={85}>
            <Card actions={[{tag: true, content: product.status, onAction: () => setOtherStatus()}]} title="Product status" sectioned>
                <Select 
                    label="Catalog visibility"
                    name="stock-status"
                    options={shop_visibility}
                    onChange={(e) => updateProductData('catalog_visibility',e.target.value)}
                    value={product.catalog_visibility}
                />
                <footer>
                { product.permalink && <a target="_blank" rel="noopener noreferrer" href={product.permalink} class="btn btn--outline">View on store</a>  }
                </footer>
            </Card>
            <Card title="Categories" sectioned>
              <CollectionTagger />
            </Card>
            <Card title="Tags" sectioned></Card>
          </Layout.Section>
        </Layout>
      </Page>
    </div>
    }
    </>
  );
};


export default connect(
  (state) => ({
    editingProduct: editingProductSelector(state),
  }),
  {
    setEditingProduct,
  }
)(newProduct);



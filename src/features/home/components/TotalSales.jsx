import React, { useEffect, useState } from 'react'
import SkeletonDisplayText from '../../../components/structure/ActivityIndicator/SkeletonDisplayText'
import Card from '../../../components/structure/card/Card'
import DisplayText from '../../../components/structure/text/DisplayText'

export const LoadingComponent = () => {
    return (
        <div className="u-flex u-flex--sb u-flex--center-v">
            <SkeletonDisplayText size="large" />
            <SkeletonDisplayText />
        </div>
    )
}

const TotalSales = () => {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
      
        setTimeout(() => {
            setIsLoading(false)
        }, 4000);

        return () => { }
    }, [])

    return (
      <Card title="Ventes totales" sectioned>
        {isLoading ? (
          <LoadingComponent />
        ) : (
          <div className="u-flex u-flex--sb u-flex--center-v">
            <DisplayText large>$34,152</DisplayText>
            <DisplayText success>
              <i className="fas fa-level-up-alt" />
              2.65%
            </DisplayText>
          </div>
        )}
      </Card>
    );
}

export default TotalSales

import React, { useEffect, useState } from 'react'
import Card from '../../../components/structure/card/Card'
import { TimelineComponent } from '../../../components/structure/timeline/TimelineComponent'

const RecentActivity = () => {
    const [items, setItems] = useState(null)

    useEffect(() => {
        setTimeout(() => {
            setItems([
                { date: 'Today12:20 pm', text: 'Andrei Coman magna sed porta finibus, risus posted a new article: Forget UX Rowland' },
                { date: 'Today12:20 pm', text: 'Andrei Coman magna sed porta finibus, risus posted a new article: Forget UX Rowland' },
                { date: 'Today12:20 pm', text: 'Andrei Coman magna sed porta finibus, risus posted a new article: Forget UX Rowland' },
                { date: 'Today12:20 pm', text: 'Andrei Coman magna sed porta finibus, risus posted a new article: Forget UX Rowland' },
                { date: 'Today12:20 pm', text: 'Andrei Coman magna sed porta finibus, risus posted a new article: Forget UX Rowland' },
                { date: 'Today12:20 pm', text: 'Andrei Coman magna sed porta finibus, risus posted a new article: Forget UX Rowland' },
                { date: 'Today12:20 pm', text: 'Andrei Coman magna sed porta finibus, risus posted a new article: Forget UX Rowland' },
                { date: 'Today12:20 pm', text: 'Andrei Coman magna sed porta finibus, risus posted a new article: Forget UX Rowland' },
            ])
        }, 2000);

        return () => {

        }
    }, [])

    return (
        <Card sectioned title="Recent Activity"
        >
            <TimelineComponent items={items} />
        </Card>
    )
}

export default RecentActivity

import React, { useEffect, useState } from 'react'
import SkeletonDisplayText from '../../../components/structure/ActivityIndicator/SkeletonDisplayText'
import Card from '../../../components/structure/card/Card'
import DisplayText from '../../../components/structure/text/DisplayText'

export const LoadingComponent = () => {
    return (
        <div className="u-flex u-flex--sb u-flex--center-v">
            <SkeletonDisplayText size="large" />
            <SkeletonDisplayText />
        </div>
    )
}

const AverageCart = () => {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false)
        }, 3000);
        return () => { }
    }, [])

    return (
      <Card title="Panier moyen" sectioned>
        {isLoading ? (
          <LoadingComponent />
        ) : (
          <div className="u-flex u-flex--sb u-flex--center-v">
            <DisplayText large>$40</DisplayText>
            <DisplayText negative>
              <i className="fas fa-level-down-alt" />
              10.00%
            </DisplayText>
          </div>
        )}
      </Card>
    );
}

export default AverageCart

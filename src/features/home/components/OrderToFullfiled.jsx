import React, { useState, useEffect } from 'react'
import Card from '../../../components/structure/card/Card'
import DataTable from '../../../components/structure/Table/DataTable'

const OrderToFullfiled = ({api}) => {

    const [rows , setRows] = useState(null);

    useEffect(() => {
        api.DashboardService.getLastOrders()
        .then((data) => {
            let rows = [];
            data.map( row => {
                rows = [...rows, [`#${row.id}`,row.date_created,row.billing.email,row.payment_method_title, row.line_items.length]]
            })
            setRows(rows)
        });
        return () => {};
    }, [api]);


    return (
        <Card
            title="Commande à traiter"
            primaryFooterAction={{ content: "Traiter les commandes", url: '/orders?status=unfullfiled' }}
        >
            <Card.Section maxHeight="460px" scrollable>
                <DataTable
                    loading={[
                        ['large', 'large', 'large', 'large', 'large'],
                        ['large', 'large', 'large', 'large', 'large'],
                        ['large', 'large', 'large', 'large', 'large'],
                        ['large', 'large', 'large', 'large', 'large'],
                        ['large', 'large', 'large', 'large', 'large'],
                        ['large', 'large', 'large', 'large', 'large'],
                    ]}
                    columnContentTypes={[
                        'text',
                        'text',
                        'text',
                        'tag',
                        'text',
                    ]}
                    headings={[
                        'Order ID',
                        'Date',
                        'Client',
                        'Paiement',
                        'Articles',
                    ]}
                    rows={rows}
                    fetching={ rows === null }
                />
            </Card.Section>
        </Card>
    )
}

export default OrderToFullfiled

// import { WooCommerce } from "../../../conf/WooCommerceApi";



export const REQUEST_HOME = "REQUEST_HOME";
export const FETCH_HOME = "FETCH_HOME";
export const FETCH_HOME_SUCCESS = "FETCH_HOME_SUCCESS";
export const FETCH_HOME_ERROR = "FETCH_HOME_ERROR";
// ASYNC ACTIONS
export const TRY_ADD_POST = "TRY_ADD_POST";

export const requestHome = () => ({
  type: REQUEST_HOME,
});

export const fetchHomeSucess = (data) => ({
  type: FETCH_HOME_SUCCESS,
  data,
});

export const fetchHomeError = (error) => ({
  type: FETCH_HOME_ERROR,
  error,
});

export const resetHome = () => async (dispatch) => {
  dispatch(fetchHomeSucess([]));
};

export const fetchHome = (dashboard) => async (dispatch) => {
  dispatch(requestHome());
  dashboard
    .getLastOrders(["pending", "on-hold"])
    .then((data) => dispatch(fetchHomeSucess(data)));
  // const data = {
  //   name: "Premium Quality",
  //   type: "variable",
  //   regular_price: "21.99",
  //   description:
  //     "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.",
  //   short_description:
  //     "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
  //   categories: [
  //     {
  //       id: 9,
  //     },
  //     {
  //       id: 14,
  //     },
  //   ],
  //   images: [
  //     {
  //       src:
  //         "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg",
  //     },
  //     {
  //       src:
  //         "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_back.jpg",
  //     },
  //   ],
  // };

  // WooCommerce.postAsync("products", data)
  //   .then((response) => {
  //     console.log(response.data);
  //   })
  //   .catch((error) => {
  //     console.log(error.response.data);
  //   });
};

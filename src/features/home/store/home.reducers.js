import initialState from "./initialState";
import * as actions from './home.actions';

export default (state = initialState, action) => {
    switch (action.type) {
        case actions.REQUEST_HOME: {
            return {
                ...state,
                isLoading: true
            }
        }
        case actions.FETCH_HOME_SUCCESS: {
            return {
                ...state,
                isLoading: false,
                error: null,
                data: action.data
            }
        }
        case actions.FETCH_HOME_ERROR: {
            return {
                ...state,
                isLoading: false,
                error: action.error,
            }
        }
        default: {
            return state
        }
    }
}
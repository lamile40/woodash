import { createSelector } from 'reselect';

export const homeSelector = state => state.home;

export const homeIsLoadingSelector = createSelector(
    [ homeSelector ],
    home => home.isLoading
)

export const homeDataSelector = createSelector(
    [homeSelector],
    home => home.data
)

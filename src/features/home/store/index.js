import { injectReducer } from "../../../stores";
import homeReducers from "./home.reducers";

injectReducer("home", homeReducers);

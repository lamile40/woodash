import React, { useEffect } from "react";
import "./store/index";
import { connect } from "react-redux";
import { fetchHome } from "./store/home.actions";
import {
  homeIsLoadingSelector,
  homeDataSelector,
} from "./store/home.selectors";
// import { Layout, Card, Page } from "@shopify/polaris";
import { UseApi } from "../../hooks/UseApi";
import Page from "../../components/structure/Page";
import Layout from "../../components/structure/layout/Layout";
import Card from "../../components/structure/card/Card";
import DisplayText from "../../components/structure/text/DisplayText";
import TotalSales from "./components/TotalSales";
import TotalOrders from "./components/TotalOrders";
import NewCustomers from "./components/NewCustomers";
import AverageCart from "./components/AverageCart";
import RecentActivity from "./components/RecentActivity";
import OrderToFullfiled from "./components/OrderToFullfiled";
import { getProducts, WpApi } from "../../api/wordpress/WordPressApi";
import { StoreContext } from "../../context/StoreContext";


function index(props) {
  
  const api = UseApi(props.store);
  
  return (
    <Page title="Dashboard">
      <Layout>
        <Layout.Section oneThird>
          <TotalSales />
        </Layout.Section>
        <Layout.Section oneThird>
          <TotalOrders />
        </Layout.Section>
        <Layout.Section oneThird>
          <NewCustomers />
        </Layout.Section>
        <Layout.Section oneThird>
          <AverageCart />
        </Layout.Section>
      </Layout>
      <Layout>
        <Layout.Section>
          <OrderToFullfiled api={api} />
        </Layout.Section>
        <Layout.Section secondary>
          <RecentActivity />
          <Card sectioned title="Top Selling Products"></Card>
          <Card sectioned title="Top customers"></Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
}

export default connect(
  (state) => ({
    isLoading: homeIsLoadingSelector(state),
    data: homeDataSelector(state),
  }),
  {
    fetchHome,
  }
)(index);

import React from "react";
import Loadable from "react-loadable";
import { AppLoader } from "../../components/utils/Loading";

const LazyOrders = Loadable({
  loader: () => import(/* webpackChunkName: 'orders' */ "./index"),
  loading: () => <AppLoader title="orders" />,
});

export default LazyOrders;

import { injectReducer } from "../../../stores";
import homeReducers from "./orders.reducers";

injectReducer("orders", homeReducers);

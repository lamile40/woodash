import { createSelector } from "reselect";

export const ordersSelector = (state) => state.orders;

export const ordersIsLoadingSelector = createSelector(
  [ordersSelector],
  (orders) => orders.isLoading
);

export const orderDataSelector = createSelector(
  [ordersSelector],
  (orders) => orders.data
);

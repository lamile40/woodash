import initialState from "./initialState";
import * as actions from "./orders.actions";

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_ORDERS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        error: null,
        data: action.data,
      };
    }
    default: {
      return state;
    }
  }
};

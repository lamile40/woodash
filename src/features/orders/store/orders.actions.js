import { WooCommerce } from "../../../conf/WooCommerceApi";

export const REQUEST_ORDERS = "REQUEST_ORDERS";
export const FETCH_ORDERS = "FETCH_ORDERS";
export const FETCH_ORDERS_SUCCESS = "FETCH_ORDERS_SUCCESS";
export const FETCH_ORDERS_ERROR = "FETCH_ORDERS_ERROR";

// ASYNC ACTIONS
export const TRY_ADD_ORDER = "TRY_ADD_ORDER";

export const requestOrders = () => ({
  type: REQUEST_ORDERS,
});

export const fetchOrdersSucess = (data) => ({
  type: FETCH_ORDERS_SUCCESS,
  data,
});

export const fetchOrdersError = (error) => ({
  type: FETCH_ORDERS_ERROR,
  error,
});

export const fetchOrders = (api) => async (dispatch) => {
  dispatch(requestOrders());
  api.OrderService.fetchOrders().then(data => dispatch(fetchOrdersSucess(data)))
};

import React from "react";
import "./store/index";
import { useEffect } from "react";
import { connect } from "react-redux";
import { fetchOrders } from "./store/orders.actions";
import ListingContainer from "../../components/shared/Lists/ListingContainer";
import HeadingList from "../../components/shared/Lists/HeadingList";
import {
  ordersIsLoadingSelector,
  orderDataSelector,
} from "./store/orders.selectors";
import { StoreContext } from "../../context/StoreContext";
import ListingContent from "../../components/shared/Lists/ListingContent";
import FilterList from "../../components/shared/Lists/FilterList";
import Page from "../../components/structure/Page";
import Layout from "../../components/structure/layout/Layout";
import { UseApi } from "../../hooks/UseApi";

const index = (props) => {

  const api = UseApi(props.store)

  useEffect(() => {
    props.fetchOrders(api);
    return () => {};
  }, [props]);

  return (
    <Page
      title="Orders"
      primaryAction={{ content: "Create order", url: "/orders/new" }}
      secondaryActions={[
        {
          content: "Export",
          onAction: () => alert("Export action"),
        },
        { content: "Import", onAction: () => alert("Import action") },
      ]}
    >
      <Layout>
        <Layout.Section>
          <FilterList inputPlaceholder="Filtrer les commandes" />
          <ListingContainer>
            <HeadingList
              entries={[
                "Order",
                "Date",
                "Client",
                "Total",
                "Payment method",
                "Order status",
              ]}
            />
            {props.data ? (
              <ListingContent>
                {props.data.map((order, index) => (
                  <tr key={`order-line-${index}`}>
                    <td>#{order.id}</td>
                    <td>{order.date_created}</td>
                    <td>
                      {order.billing.first_name} {order.billing.last_name}
                    </td>
                    <td>{order.total}</td>
                    <td>{order.payment_method}</td>
                    <td>{order.status}</td>
                  </tr>
                ))}
              </ListingContent>
            ) : (
              <p
                style={{ width: "100%", textAlign: "center", display: "flex" }}
              >
                loading...
              </p>
            )}
          </ListingContainer>
        </Layout.Section>
      </Layout>
    </Page>
  );
};

export default connect(
  (state) => ({
    isLoading: ordersIsLoadingSelector(state),
    data: orderDataSelector(state),
  }),
  {
    fetchOrders,
  }
)(index);

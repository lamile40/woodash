import React from "react";
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import cx from "classnames/bind";
import { Link, withRouter } from "react-router-dom";
import { fetchUserSucess } from "../../../stores/actions";
import { connect } from "react-redux";
import LocalStorageInstance from "../../../helpers/LocalStorage";

const ComposantErreur = props => {
  return (
    <div className="error-message">
      <span>{props.children}</span>
    </div>
  );
};

const ComposantInput = ({ field, form: { touched, errors }, ...props }) => {
  return (
    <div className={cx("form-group", { invalid: errors[field.name] })}>
      <label> {props.label} </label>
      <input type="text" {...props} className="form-control" {...field} />
    </div>
  );
};

export const Login = props => {
  const submit = async (values, actions) => {
    props.fetchUserSucess(values)
    LocalStorageInstance.store('user', JSON.stringify(values))
  };

  const userSchema = Yup.object().shape({
    username: Yup.string().required("Champs obligatoire"),
    password: Yup.string().required("Champs obligatoire")
  });

  return (
    <Formik
      onSubmit={submit}
      initialValues={{ username: "", password: "" }}
      validationSchema={userSchema}
      validateOnChange={true}
    >
      
      {({ handleSubmit, isSubmitting }) => (
        <form
          onSubmit={handleSubmit}
          className="w-50 bg-white border p-5 d-flex flex-column"
        >
          <Field name="username" label="Email" component={ComposantInput} />
          <ErrorMessage name="username" component={ComposantErreur} />
          <Field
            name="password"
            type="password"
            label="Mot de passe"
            component={ComposantInput}
          />
          <ErrorMessage name="password" component={ComposantErreur} />
          <button
            type="submit"
            className="btn btn--primary"
            disabled={isSubmitting}
          >
            Se connecter
          </button>
          <span className="form_link text-center">
            <Link to="/auth/register">Inscription</Link>
            <span className="sep">|</span>
            <Link to="/auth/forgot-password">Mot de passe oublié?</Link>
          </span>
        </form>
      )}
    </Formik>
  );
};

export default withRouter( 
  connect(
    () => ({}),
    {
      fetchUserSucess
    }
  )
  (Login)
);

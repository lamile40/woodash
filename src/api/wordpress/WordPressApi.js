import axios from "axios";
import { WooCommerce } from "../../conf/WooCommerceApi";

class WordPressApi {
  constructor(store) {
    this.woocommerce = WooCommerce(store);
    this.wordpress = this.getApiService(store);
  }

  getApiService(store) {
    const apiService = axios.create({
      baseURL: store.url + "wp-json/",
    });

    apiService.interceptors.request.use(
      (config) => {
        config.headers.Authorization = this._basicAuth(
          store.consumerKey,
          store.consumerSecret
        );

        return config;
      },
      (error) => {
        return Promise.reject(error);
      }
    );

    return apiService;
  }

  _basicAuth(key, secret) {
    let hash = btoa(key + ":" + secret);
    return "Basic " + hash;
  }
}

export default WordPressApi;

import WordPressApi from "../../wordpress/WordPressApi";

export default class ProductService extends WordPressApi {
  constructor(store) {
    super(store);
  }

  fetchProducts() {
    return new Promise((resolve) => {
      this.woocommerce
        .getAsync(`products`)
        .then((res) => resolve(JSON.parse(res.body)));
    });
  }

  /**
   * Créer un produit woocommerce
   * @param {object} data
   */
  create(data) {
    return new Promise((resolve) => {
      this.woocommerce
        .postAsync("products", data)
        .then((res) => resolve(JSON.parse(res.body)));
    });
  }

  /**
   * Charges les donnée d'un produot
   * @param {string} id
   */
  get(id) {
    return new Promise((resolve) => {
      this.woocommerce
        .getAsync(`products/${id}`)
        .then((res) => resolve(JSON.parse(res.body)));
    });
  }

  edit(id, data) {
    return new Promise((resolve) => {
      this.woocommerce
        .putAsync(`products/${id}`, data)
        .then((res) => resolve(JSON.parse(res.body)));
    });
  }

  batchCreateVariants( product , data) {

    return new Promise((resolve) => {
      this.woocommerce
        .postAsync(`products/${product.id}/variations/batch`, data)
        .then((res) => resolve(JSON.parse(res.body)));
    });

  }

  batchDeleteVariants( product , data) {

    return new Promise((resolve) => {
      this.woocommerce
        .postAsync(`products/${product.id}/variations/batch`, data)
        .then((res) => resolve(JSON.parse(res.body)));
    });

  }

  getProductVariations( pid ) {
    return new Promise((resolve) => {
      this.wordpress
        .get(`wc/v3/products/${pid}/variations`,{params :{per_page : 100}})
        .then((res) => resolve(res.data));
    });
  }

  updateProductVariation( pid, data ) {
    return new Promise((resolve) => {
      this.wordpress
        .put(`wc/v3/products/${pid}/variations/${data.id}`, data )
        .then((res) => resolve(res.data));
    });
  }

  uploadMedias(data) {
    return new Promise((resolve) => {
      this.wordpress
        .post(`wc/v2/media`, data )
        .then((res) => resolve(res.data));
    });
  }

  updateProduct(id,data) {
    return new Promise((resolve) => {
      this.woocommerce
        .putAsync(`products/${id}`, data )
        .then((res) => resolve(res.data));
    });
  }

}

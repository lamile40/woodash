import WordPressApi from "../../wordpress/WordPressApi";

export default class OrderApi extends WordPressApi {
    
  constructor(store) {
    super(store);
  }

  /**
   * Créer un produit woocommerce
   * @param {object} data
   */
  fetchOrders() {
    return new Promise((resolve) => {
      this.woocommerce
        .getAsync("orders")
        .then((res) => resolve(JSON.parse(res.body)));
    });
  }

}

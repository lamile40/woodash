import WordPressApi from "../../wordpress/WordPressApi";

export default class DashboardApi extends WordPressApi{

  constructor(store) {
    super(store)
  }

  getTest() {
    // console.log(this)
    return new Promise((resolve) => {
      this.wordpress
        .get(
          "wc-analytics/reports/revenue/stats?after=2020-01-01T00%3A00%3A00&before=2020-10-08T23%3A59%3A59&fields%5B0%5D=orders_count&fields%5B1%5D=gross_sales&fields%5B2%5D=total_sales&fields%5B3%5D=refunds&fields%5B4%5D=coupons&fields%5B5%5D=taxes&fields%5B6%5D=shipping&fields%5B7%5D=net_revenue&interval=day&order=asc&_locale=user"
        )
        .then( res =>resolve(res));
    });
  }

  getLastOrders( status = ["pending", "on-hold", "completed"] ) {
    return new Promise((resolve) => {
      this.wordpress.get("wc/v3/orders",{ params: { status } })
      .then( res => resolve(res.data) )
    });
  }
  
}



import { ProductService, DashboardService, OrderService } from "../api/woocommerce";
import { DashboardShopifyService } from "../api/shopify";

export const UseApi = ( store  ) => {
  
    if( store ) {
      switch (store.provider) {
        case "woocommerce":
          return {
            DashboardService: new DashboardService(store),
            ProductService: new ProductService(store),
            OrderService: new OrderService(store),
          };
        default:
          return { DashboardService: new DashboardShopifyService(store) };
      }
    }      
    
    return null
  
};

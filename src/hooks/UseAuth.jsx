import React from "react";
import { AuthContext } from "../context/AuthContext";

export const UseAuth = () => {
  return React.useContext(AuthContext);
};

import React from "react";
import LocalStorageInstance from "../helpers/LocalStorage";

function UseCustomAPIAuth() {
  const [user, setUser] = React.useState();

  const signin = async (values, actions) => {
    setUser(values)
    LocalStorageInstance.store('user', JSON.stringify(values))
  };

  const signup = async (values) => {
    LocalStorageInstance.store('user', JSON.stringify(values))
    setUser(values)
  };

  const signout = () => {
    setUser(null);
  };

  const sendPasswordResetEmail = async (email) => {};

  const confirmPasswordReset = (values) => {};

  React.useEffect(() => {
    setUser(LocalStorageInstance.get('user') ? LocalStorageInstance.get('user') : null)
  }, [user]);

  // Return the user object and auth methods
  return {
    user,
    signin,
    signup,
    signout,
    sendPasswordResetEmail,
    confirmPasswordReset,
  };
}

export default UseCustomAPIAuth;

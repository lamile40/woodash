import React from "react";

const initialState = null;

const StoreContext = React.createContext();

const StoreProvider = (props) => {
  
  const [store, setStore] = React.useState(initialState);

  return (
    <StoreContext.Provider value={[store, setStore]}>
      {props.children}
    </StoreContext.Provider>
  );
};

export { StoreContext, StoreProvider };

import React from "react";

const ModalContext = React.createContext();

const defaultState = {
  display: false,
  component: null,
  props: {}
};

const ModalProvider = ({ children }) => {
  const [modalData, setModalData] = React.useState(defaultState);
  return (
    <ModalContext.Provider value={[modalData, setModalData]}>
      {children}
    </ModalContext.Provider>
  );
};

export { ModalContext, ModalProvider };
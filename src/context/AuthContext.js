import React from "react";

const AuthContext = React.createContext();

const AuthProvider = ({ children, type }) => {
  console.log(type)
  return <AuthContext.Provider value={type()}>{children}</AuthContext.Provider>;
};

export { AuthContext, AuthProvider };

import React, {Component} from 'react'
import { withRouter } from 'react-router-dom'
import Auths from '../auth';
import * as _ from "lodash"
import LocalStorageInstance from '../helpers/LocalStorage';
import { toast } from 'react-toastify';

export const AuthContext = React.createContext()

class AuthProvider extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isAuth: LocalStorageInstance.get('authUser') || null ,
      authData : null,
      error: null,
      success: null
    }

    let AuthChoice = _.find( Auths, { type: this.props.provider })
    this.auth = _.isUndefined(AuthChoice) ? undefined : new AuthChoice.model()
    
  }

  componentWillUpdate(nextProps, nextState) {
    if( nextState.isAuth !== this.state.isAuth ) {
      if( nextState.isAuth === null ) {
        toast.info('Vous avez été déconnecté')
      }
    }
  }
  

  call = ( method, params = {} ) => {
    !_.isUndefined(this.auth) && this.auth[method]( params, this )
  }

  setAuth = (value) => {
    this.setState({isAuth:value})
  }

  authenticate = (data) => {
    LocalStorageInstance.store( 'authUser', data.token )
    this.setState({isAuth:true})
  }
 

  render() {

    return (   
      <AuthContext.Provider
        value={{
          isAuth: this.state.isAuth,
          ...this.state,
          call: this.call,
          setAuth: this.setAuth
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    )

  }
}


export default withRouter(AuthProvider)

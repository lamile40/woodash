import WooCommerceAPI from "woocommerce-api";

export const WooCommerce = (store) => {
  return new WooCommerceAPI({
    url: store.url,
    consumerKey: store.consumerKey,
    consumerSecret: store.consumerSecret,
    wpAPI: true,
    version: "wc/v3",
  });
};

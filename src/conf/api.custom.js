import axios from "axios";
import LocalStorageInstance from "../helpers/LocalStorage";

const apiService = axios.create({
  baseURL: process.env.REACT_APP_API_URL
});

/**
 * Ajout automatique du token sur l'ensemble des requête effectué avec AXIOS
 */
apiService.interceptors.request.use(
  config => {
    const token = LocalStorageInstance.get("authUser");
    if (token != null) {
      config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

/**
 * Gestion des cas d'erreur renvoyé par l'API
 */
apiService.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    // On traite ici les différent type d'erreur reçu via l'api
    if (error.status) {
      switch (error.response.status) {
        // TODO: Possibilité de mettre en place un refresh token
        case 401:
          // Aucun droit pour exécuter la requête ( On deconnecte et redirige vers le login )
          break;
        case 404:
          // Information Request not found
          break;
        default:
          break;
      }
    } else {
      // toast.error("Une erreur interne vient de se produire.", {
      //     position: toast.POSITION.BOTTOM_CENTER
      // });
    }

    return Promise.reject(error.response);
  }
);

export default apiService;

import React from 'react'
import { Switch, Route, Redirect } from "react-router-dom";
import { routes } from "./routing/routes";
import { PrivateRoute } from "./routing/PrivateRoute";
import { NotFound } from "./components/shared/not-found/NotFound";

export const AppNoUserEntry = props => {

    const [redirect, setredirect] = React.useState(false)

    React.useEffect(() => {
      if(props.default_store === null) {
        setredirect(true)
      }
      return () => {
        
      }
    }, [props])

    return (
        <div className="welcome">
            {redirect && <Redirect to='/add-your-store' />}
            <Switch>
            {
              routes.map((route) => {
                return route.private ? (
                   route.sub_menu && route.sub_menu.length > 0 ? 
                   route.sub_menu.map( subroute =>
                    <PrivateRoute
                      key={subroute.path}
                      component={subroute.component}
                      path={subroute.path}
                      exact
                      user={props.user}
                      {...subroute}
                    />
                  )
                   :
                    <PrivateRoute
                      key={route.path}
                      component={route.component}
                      path={route.path}
                      exact
                      user={props.user}
                      {...route}
                    />
                ) : (
                  <Route
                    key={route.path}
                      render={(props) => (
                        <route.component {...props} {...route} />
                      )}
                    path={route.path}
                    exact={route.exact}
                  />
                );
              })
            }
            <Route exact path="/">
              <Redirect to="/dashboard" />
            </Route>
            <Route component={NotFound} />
          </Switch>
        </div>
    )
}

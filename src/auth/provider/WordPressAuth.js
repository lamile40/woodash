import base from "../base";
import apiService from "../../conf/api.custom";
import UserService from "../../api/services/UserService";

export default class WordPressAuth extends base {

    /** 
     * Call API permettant de se logguer
     */
    login = ( {actions, values, ...rest}, AuthContextClass ) => {
        UserService
            .login('v1/token',values)
            .then( response => {
                actions.setSubmitting(false);
                AuthContextClass.authenticate( response )
            })
            .catch( error => console.log(error) ) 
    } 

    /** 
     * Call API permettant de s'inscrire
     */
    register = ( {actions, values, ...rest}, AuthContextClass ) => {
        // TODO: implement register method from custom API
        apiService
            .post('wp/v2/users', {
                username : values.email,
                email: values.email,
                password: values.password,
                roles: ['subscriber']
            })
            .then( response => {
                // On connecte le nouvel inscrit
                if( response.status === 201 ) {
                    UserService
                    .login('v1/token',{
                        username : values.email,
                        password: values.password,
                    })
                    .then( response => {
                        actions.setSubmitting(false);
                        AuthContextClass.authenticate( response )
                    })
                    .catch( error => console.log(error))
                } 
            });
    }

    /** 
     * Call API permettant de re-intialiser son mot de passe
     */
    forgotPassword = ( {actions, values, ...rest}, AuthContextClass ) => {
        // TODO: implement register method from custom API
        UserService
            .resetPassword(values)
            .then( () => {
                actions.setSubmitting(false)
            })
            .catch( () => actions.setSubmitting(false) )
    }

}
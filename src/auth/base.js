export default class base  {

    logout = ( args , context) => {
        localStorage.removeItem('authUser')
        context.setState({ isAuth: null })
        context.props.history.push('/auth/login')
    }

}


export const AUTH_METHOD = {
    LOGIN: 'login',
    REGISTER: 'register',
    FORGOT_PASSWORD: 'forgotPassword',
    LOGOUT: 'logout'
}
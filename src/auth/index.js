import CustomAuth from "./provider/CustomAuth";
import WordPressAuth from "./provider/WordPressAuth";

export default [
    {
        type: 'custom',
        model: CustomAuth
    },
    {
        type: 'wordpress',
        model: WordPressAuth
    }
]
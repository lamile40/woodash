import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import store from "./stores";
import { BrowserRouter as Router } from "react-router-dom";
import HTML5Backend from "react-dnd-html5-backend";
import { DragDropContextProvider } from "react-dnd";
import "@shopify/polaris/dist/styles.css";
import enTranslations from "@shopify/polaris/locales/en.json";
import { AppProvider } from "@shopify/polaris";
import { StoreProvider } from "./context/StoreContext";
import { ToastProvider } from 'react-toast-notifications'
import { ModalProvider } from "./context/ModalContext";

ReactDOM.render(
  <Provider store={store}>
    <Router>
        <DragDropContextProvider backend={HTML5Backend}>
          <AppProvider i18n={enTranslations}>
            <StoreProvider>
              <ToastProvider placement="bottom-center" autoDismiss={true} autoDismissTimeout={3500}>
                <ModalProvider>
                  <App />
                </ModalProvider>
              </ToastProvider>
            </StoreProvider>
          </AppProvider>
        </DragDropContextProvider>
    </Router>
  </Provider>,
  document.getElementById("root")
);

import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

export const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <>
      <Route
        {...rest}
        render={props =>
          rest.user ? (
            <Component {...props} {...rest} />
          ) : (
            <Redirect
              to={{
                pathname:
                  rest.routeProps && rest.routeProps.auth_redirect_path
                    ? rest.routeProps.auth_redirect_path
                    : "/auth/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    </>
  );
};

export default connect(
  
)(PrivateRoute)


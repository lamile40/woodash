import React,{useState,useEffect, useContext} from "react";
import ScrollToTop from "./ScrollToTop";
import { connect } from "react-redux";
import { withRouter, Redirect, useLocation } from "react-router-dom";
import { AppUserSelector, defaultStoreSelector } from "../stores/selectors";
import { UseAuth } from "../hooks/UseAuth";
import AppEntry from "../AppEntry";
import { AppNoUserEntry } from "../AppNoUserEntry";
import { StoreContext } from "../context/StoreContext";

const AppRouter = (props) => {

  const [entry, setEntry] = useState(''); // your state value to manipulate
  const [store, setStore] = useState(''); // your state value to manipulate

  useEffect(() => {   
    
    if( props.user && props.default_store === null ) {
        setEntry('add-store')
    } 

    setStore(props.default_store)

    return () => {}
  }, [props.user, props.default_store])


  return (
    <>
    <ScrollToTop>
        {entry === 'add-store' && <Redirect to={'/add-your-store'} /> }
        <div className="app">
        { 
          props.user && store
          ? 
            <AppEntry user={props.user} default_store={store} /> 
          : 
            <AppNoUserEntry user={props.user} default_store={store}/>  } 
        </div> 
    </ScrollToTop>
    </>
  );
};

export default withRouter(
  connect(
    (state) => ({
    user: AppUserSelector(state),

    default_store: defaultStoreSelector(state),
    })
  )(AppRouter)
);

import Auth from "../features/auth";
// import LazyPosts from "../features/posts/Lazy";
import LazyHome from "../features/home/Lazy";
import DashboardIcon from "./../assets/images/dashboardIcon.svg";
import LazyOrders from "../features/orders/Lazy";
import LazyProducts from "../features/products/Lazy";
import newProduct from "../features/products/newProduct";
import LazyEmails from "../features/marketing/emails/Lazy";
import editEmail from "../features/marketing/emails/edit";
import AddStore from "../components/shared/add-store/AddStore";

export const routes = [
  {
    path: "/dashboard",
    private: true,
    component: LazyHome,
    exact: false,
    routeProps: {
      main_nav: true,
      main_nav_label: "Dashboard",
      icon: "fas fa-tachometer-alt",
    },
    sub_menu: [],
  },
  {
    path: "/orders",
    private: true,
    component: LazyOrders,
    exact: true,
    routeProps: {
      main_nav: true,
      main_nav_label: "Orders",
      icon: "fas fa-cash-register",
    },
    sub_menu: [
      {
        path: "/orders/checkout",
        private: true,
        label: "Abandoned checkouts",
      },
    ],
  },
  {
    path: "/products/new",
    private: true,
    component: newProduct,
    exact: true,
  },
  {
    path: "/products/:id",
    private: true,
    component: newProduct,
    exact: true,
  },
  {
    path: "/products",
    private: true,
    component: LazyProducts,
    exact: true,
    routeProps: {
      main_nav: true,
      main_nav_label: "Products",
      icon: "fas fa-tags",
    },
    sub_menu: [
      {
        path: "/products/categories",
        private: true,
        label: "Collections",
      },
      {
        path: "/products/tags",
        private: true,
        label: "Tags",
      },
    ],
  },
  {
    path: "/customers",
    private: true,
    component: LazyHome,
    exact: true,
    routeProps: {
      main_nav: true,
      main_nav_label: "Clients",
      icon: "fas fa-users",
    },
    sub_menu: [],
  },
  {
    path: "/marketing",
    private: true,
    component: 'LazyHome',
    exact: true,
    routeProps: {
      main_nav: true,
      main_nav_label: "Marketing",
      icon: "fas fa-mail-bulk",

    },
    sub_menu: [
      {
        path: "/marketing/emails",
        private: true,
        label: "Lists",
      },
      {
        path: "/marketing/emails",
        private: true,
        label: "Emails",
      },
      {
        path: "/marketing/campaigns",
        private: true,
        label: "Campaigns",
      },
      {
        path: "/marketing/automation",
        private: true,
        label: "Automation",
      },
      {
        path: "/marketing/discounts",
        private: true,
        label: "Coupon",
      }
    ],
  },
  {
    path: "/add-your-store",
    private: true,
    component: AddStore,
    exact: true,
  },
  {
    path: "/auth/:action",
    private: false,
    component: Auth,
    exact: true,
  },
];

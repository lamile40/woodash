import React, {useState, useEffect} from 'react'
import { Switch, Route, Redirect } from "react-router-dom";
import { routes } from "./routing/routes";
import { PrivateRoute } from "./routing/PrivateRoute";
import { NotFound } from "./components/shared/not-found/NotFound";
import SidebarMenu from "./components/shared/sidebar-menu/SidebarMenu";
import { Header, Footer } from "./components";

const AppEntry = props => {


    return (
      props.default_store && 
        <>
        <Header /> 
        <div className="content-right">
          <SidebarMenu default_store={props.default_store} />
          <main className="content">
            <Switch>
              {
                routes.map((route) => {
                  return route.private ? (
                    route.sub_menu && route.sub_menu.length > 0 ? 
                    route.sub_menu.map( subroute =>
                      <PrivateRoute
                        key={subroute.path}
                        component={subroute.component}
                        path={subroute.path}
                        exact
                        user={props.user}
                        store={props.default_store}
                        {...subroute}
                      />
                    )
                    :
                      <PrivateRoute
                        key={route.path}
                        component={route.component}
                        path={route.path}
                        exact
                        user={props.user}
                        store={props.default_store}
                        {...route}
                      />
                  ) : (
                    <Route
                      key={route.path}
                        render={(props) => (
                          <route.component {...props} {...route} />
                        )}
                      path={route.path}
                      exact={route.exact}
                    />
                  );
                })
              }
              <Route exact path="/">
                <Redirect to="/dashboard" />
              </Route>
              <Route component={NotFound} />
            </Switch>
          </main>
          </div>
        <Footer />
        </> 
     
    )
}

export default AppEntry

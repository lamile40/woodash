import apiService from "../../conf/api.custom";

export const REQUEST_USER = 'get user';
export const FETCH_USER = 'fetch user';
export const FETCH_USER_SUCCESS = 'fetch user success';
export const FETCH_USER_ERROR= 'fetch user error';
export const ADD_STORE = 'ADD_STORE';
export const SWITCH_STORE = 'SWITCH_STORE';


export const requestUser = () => ({
    type: REQUEST_USER,
});

export const addStore = (data) => ({
    type: ADD_STORE,
    payload: data
});

export const switchStore = (name) => ({
    type: SWITCH_STORE,
    payload: name
});

export const fetchUserSucess = user => ({
    type: FETCH_USER_SUCCESS,
    user
});

export const fetchUser = () => dispatch => {
    dispatch( requestUser() );
    return apiService.getUser().then(
        user => dispatch( fetchUserSucess(user) ),
        error => dispatch( fetchUserError(error.response) )
    )
}

export const fetchUserError = error => ({
    type: FETCH_USER_ERROR,
    error
});


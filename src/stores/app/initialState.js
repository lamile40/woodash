import LocalStorageInstance from "../../helpers/LocalStorage";

export default {
  user: LocalStorageInstance.get('user') || null ,
  default_store: LocalStorageInstance.get('default_store') || null,
  stores: LocalStorageInstance.get('stores') ? [JSON.parse(LocalStorageInstance.get('stores'))] : [],
  stores: [
    {
      name: "Wc SHOP demo",
      url: "https://wcshop.local/",
      consumerKey: "ck_46cce74918a12757b92719cce64c3f3082947fbe",
      consumerSecret: "cs_8405228ec6a131eefd3019456fea62b1c337adb9",
      provider: "woocommerce",
    },
    {
      name: "Willo",
      url: "https://willow.local/",
      consumerKey: "ck_0012ecc5651609890ea697cfee4f3e2528617216",
      consumerSecret: "cs_ecbef06e53c456f5f89b85d76b323d217de0e51b",
      provider: "woocommerce",
    },
  ],
  isLoading: false,
  error: null,
};

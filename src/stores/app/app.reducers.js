import initialState from "./initialState";
import * as actions from './app.actions';

export default (state = initialState, action) => {
    switch (action.type) {
        case actions.REQUEST_USER: {
            return {
                ...state,
                isLoading: true
            }
        }
        case actions.FETCH_USER_SUCCESS: {
            return {
                ...state,
                isLoading: false,
                error: null,
                user: action.user
            }
        }
        case actions.FETCH_USER_ERROR: {
            return {
                ...state,
                isLoading: false,
                error: action.error,
            }
        }
        case actions.ADD_STORE: {
            console.log(action.payload)
            return {
                ...state,
                stores: [...state.stores, action.payload],
                default_store: action.payload.name
            }
        }
        case actions.SWITCH_STORE: {
            console.log(action.payload)
            return {
                ...state,
                default_store: action.payload
            }
        }
        default: {
            return state
        }
    }
}
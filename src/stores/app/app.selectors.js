import { createSelector } from "reselect";

export const appSelector = (state) => state.app;

export const appIsLoadingSelector = createSelector(
  [appSelector],
  (app) => app.isLoading
);

export const AppUserSelector = createSelector([appSelector], (app) => app.user);

export const defaultStoreSelector = createSelector([appSelector], (app) => {
  let store = null
  if( app.stores && app.default_store ) {
    const match =  app.stores.filter((entry) => entry.name === app.default_store);
    if (match.length > 0) store = match[0];
  }
 

  return store;
});
